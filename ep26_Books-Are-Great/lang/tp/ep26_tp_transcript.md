# Transcript of Pepper&Carrot Episode 26 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka wan: lipu li pona mute a

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|lipu li pona mute a!
jan Pepa|3|False|ni li suli a!
jan Pepa|2|True|jan wawa pi alasa musi li sitelen lon lipu ni, li toki e ale pi tomo alasa.
jan Pepa|4|False|nanpa wan la, oko ike ni li pana e seli moli tawa jan pi wile ala ona.
jan Pepa|5|False|taso sina kama tan poka la, oko li lukin ala la...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|...sina len e oko la...
jan Pepa|3|False|...lupa o kama open tan ni!
jan Pepa|4|True|jan ni la, len o awen. jan li ken weka tan tomo kepeken pali ala tan ni!
jan Pepa|5|False|lipu li pona mute a!
sitelen|6|False|“o tawa sewi pi supa nanpa wan.”
jan Pepa|7|False|pona!
kalama|9|False|TAWA
kalama|10|False|TAWWA
sitelen|8|False|“ken la, sina pilin sama kama anpa. ni li ike ala. ni li nasin pi lili mute.”
sitelen|11|False|“o pilin pona tan linja len pipi suno. ike ala li lon: pipi ni li moku e suno taso.”
sitelen|12|False|“o lape lili. o sin e wawa sina.”
jan Pepa|13|False|mi olin e lipu a!
kalama|2|False|Len!
<hidden>|0|False|This “they” means one person, without specifying their gender. If possible, please try to do the same in your language (suggestion: if your language doesn't have genderless pronouns, maybe you can translate as “the author”). If that doesn't really work well, assume the author is a woman.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|2|False|mi sona e pilin ike soweli tan telo.
jan Pepa|3|True|o pilin ike ala!
jan Pepa|4|False|lipu li pona tawa ni kin!
jan Pepa|5|True|mi kepeken tenpo lili lon tomo alasa la, mi lon tomo lili mani kin. mi ken poki e lipu kasi tan kasi telo. kasi telo li mute ala taso.
jan Pepa|6|False|lipu li wawa a!
jan Pepa|1|True|soweli Kawa o;

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen|3|False|“sina kepeken nasin lili mi la, sina kama tawa tomo pi jan utala awen.”
jan Pepa|4|True|a...
jan Pepa|5|True|jan utala?!?
jan Pepa|6|False|taso ona li lon seme?
jan Pepa|7|True|A AAA...
jan Pepa|8|False|mi lukin e sina!
jan Pepa|11|True|lili a...!
jan Pepa|12|False|ni li pali ala tan sona pi pakala sijelo!
sitelen|1|False|“o kama jo e ko palisa waso lon nasin.”
jan Pepa|2|False|ko palisa mute li lon!
jan Pepa|9|True|Musi
jan Pepa|10|False|Musi

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Sssinpin!|nowhitespace
jan Pepa|2|False|aa! pini li lon!
kalama|3|False|Aanpa!
jan Pepa|5|True|jaki a...
jan Pepa|6|False|pilin la, mi ken weka e ijo ni tan toki lipu...
jan Pepa|7|True|soweli Kawa o... sina ken lukin e lipu anu seme?
jan Pepa|8|True|a... sina sona ala e sitelen...
jan Pepa|9|False|ike a...
jan Pepa|4|False|?!
jan Pepa|12|False|ike a!!!
jan Pepa|10|True|PILIN IKE MI LI KAMA LON A!!!
jan Pepa|11|True|pona li ken ala awen lon mi!
jan Pepa|13|False|ken la, lipu li pona ala tawa ale...
kalama|14|False|Supa!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|a, taso ken la...
jan Pepa|2|False|AAA A A A A A A ! ! !|nowhitespace
kalama|3|False|U TALA !|nowhitespace
jan Pepa|5|False|...lipu li pona mute a!
jan Pepa|4|True|ala. ni li lon; tenpo ale la...
sitelen toki|6|False|- PINI -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|nasin pi lawa jo: Creative Commons Attribution 4.0. ilo: ilo Krita 4.1 en ilo Inkscape 0.92.3 li kepeken ilo Kubuntu 17.10. musi sitelen & toki: Dan David Revoy. pona pi kon toki: jan Craig Maloney. jan pi lukin pona: jan Ambernite en jan Craig Maloney en jan CalimeroTeknik en jan Jookia en jan Midgard en jan Nartance en jan Popolon en jan Quiralta en jan Valvin en jan xHire en jan Zeograd. toki pona ante toki: jan Ret Samys. musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. jan pi pona pali: jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson. tenpo 28 July 2018 www.peppercarrot.com
mama|3|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
mama|2|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 1098 li pana e mani la, lipu ni li lon:
<hidden>|0|False|Remove the names of the people who helped to create the English version and create this section for your language. You can credit translators, proofreaders, whatever categories you want. N.B.: In Inkscape, if you press Enter to create a new line, it will not push down text below it until the line has text. So to create a whiteline, type a space on its own line.
