# Transcript of Pepper&Carrot Episode 02 [mx]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodio 2 : Pociones de Arcoíris

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Glu
Sound|6|True|Glu
Sound|7|False|Glu
Writing|3|False|EMBRUJADA
Writing|2|True|PROPIEDAD
Writing|1|True|¡PELIGRO!
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|tinn|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glu
Sound|2|True|Glu
Sound|3|False|Glu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Gulp
Sound|2|False|Gulp
Sound|3|True|Gulp
Sound|4|False|Gulp
Sound|5|True|Gulp
Sound|6|False|Gulp
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|¡M|nowhitespace
Sound|7|True|¡B|nowhitespace
Sound|8|True|l|nowhitespace
Sound|9|True|a|nowhitespace
Sound|10|False|rgg !|nowhitespace
Sound|11|True|S|nowhitespace
Sound|12|True|S|nowhitespace
Sound|13|True|S|nowhitespace
Sound|14|True|P|nowhitespace
Sound|15|True|l|nowhitespace
Sound|16|True|o|nowhitespace
Sound|17|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|G
Sound|2|True|lub|nowhitespace
Sound|3|True|G
Sound|4|False|lub|nowhitespace
Sound|7|True|plof
Sound|6|True|lof|nowhitespace
Sound|5|True|p
Sound|10|False|lof|nowhitespace
Sound|9|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Esta tira cómica es de código libre. Este episodio ha tenido 21 contribuyentes en
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Gracias a :
Credits|4|False|hecho con Krita en GNU/Linux-traducido con Inkscape en Archlinux
Credits|5|False|Traducción al castellano : TheFaico adaptado para México por RJ Quiralta
