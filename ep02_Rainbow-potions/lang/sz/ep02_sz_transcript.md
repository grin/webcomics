# Transcript of Pepper&Carrot Episode 02 [sz]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Tajla 2: Dyngowe Mikstury

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Chlup
Sound|6|True|Chlup
Sound|7|False|Chlup
Writing|1|True|POZŌR!
Writing|3|False|ÔD HEKSY
Writing|2|True|DŌM
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|klup|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Chlup
Sound|2|True|Chlup
Sound|3|False|Chlup

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Szluk
Sound|2|False|Szluk
Sound|3|True|Szluk
Sound|4|False|Szluk
Sound|5|True|Szluk
Sound|6|False|Szluk
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M|nowhitespace
Sound|7|True|P|nowhitespace
Sound|8|True|f|nowhitespace
Sound|9|True|u|nowhitespace
Sound|10|False|uee !|nowhitespace
Sound|11|True|K|nowhitespace
Sound|12|True|l|nowhitespace
Sound|13|True|a|nowhitespace
Sound|14|True|p|nowhitespace
Sound|15|True|s|nowhitespace
Sound|16|True|s|nowhitespace
Sound|17|False|s|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lup|nowhitespace
Sound|3|True|B
Sound|4|False|lup|nowhitespace
Sound|7|True|ck|nowhitespace
Sound|6|True|la|nowhitespace
Sound|5|True|k
Sound|10|True|ck|nowhitespace
Sound|9|True|la|nowhitespace
Sound|8|True|k
Sound|13|False|ck|nowhitespace
Sound|12|True|la|nowhitespace
Sound|11|True|k

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Tyn kōmiks mŏ ôdewrzōne źrōdło. Ta tajla była fōndniyntŏ bez mojich 21 patrōnōw na
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Wielge podziynkowania dlŏ
Credits|4|False|stworzōne we Krita na GNU/Linux
