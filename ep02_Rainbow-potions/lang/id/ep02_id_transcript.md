# Transcript of Pepper&Carrot Episode 02 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Judul|1|False|Episode 2: Ramuan pelangi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|5|True|Blub
Suara|6|True|Blub
Suara|7|False|Blub
Penulis|1|True|WARNING
Penulis|3|False|PROPERTY
Penulis|2|True|WITCH
Penulis|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|7|False|tek|nowhitespace
Penulis|1|False|FIRE D
Penulis|2|False|DEEP OCEAN
Penulis|3|False|VIOLE(N)T
Penulis|4|False|ULTRA BLUE
Penulis|5|False|PIN
Penulis|6|False|MSON
Penulis|8|False|NATURE
Penulis|9|False|YELLOW
Penulis|10|False|ORANGE TOP
Penulis|11|False|FIRE DANCE
Penulis|12|False|DEEP OCEAN
Penulis|13|False|VIOLE(N)T
Penulis|14|False|ULTRA BLUE
Penulis|15|False|META PINK
Penulis|16|False|MAGENTA X
Penulis|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|True|Blub
Suara|2|True|Blub
Suara|3|False|Blub

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|True|Glek
Suara|2|False|Glek
Suara|3|True|Glek
Suara|4|False|Glek
Suara|5|True|Glek
Suara|6|False|Glek
Suara|20|False|m|nowhitespace
Suara|19|True|m|nowhitespace
Suara|18|True|M|nowhitespace
Suara|7|True|S|nowhitespace
Suara|8|True|P|nowhitespace
Suara|9|True|l|nowhitespace
Suara|10|False|urp !|nowhitespace
Suara|11|True|S|nowhitespace
Suara|12|True|S|nowhitespace
Suara|13|True|S|nowhitespace
Suara|14|True|P|nowhitespace
Suara|15|True|l|nowhitespace
Suara|16|True|a|nowhitespace
Suara|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Suara|1|True|B
Suara|2|True|leb|nowhitespace
Suara|3|True|B
Suara|4|False|leb|nowhitespace
Suara|7|True|up|nowhitespace
Suara|6|True|pl|nowhitespace
Suara|5|True|s
Suara|10|True|up|nowhitespace
Suara|9|True|pl|nowhitespace
Suara|8|True|s
Suara|13|False|up|nowhitespace
Suara|12|True|pl|nowhitespace
Suara|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kredit|1|True|Komik ini gratis dan didukung oleh 21 donator di Patreon
Kredit|2|False|www.patreon.com/davidrevoy
Kredit|3|False|Terima kasih kepada
Kredit|4|False|dibuat dengan Krita di GNU/Linux
