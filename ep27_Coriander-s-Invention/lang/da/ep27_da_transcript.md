# Transcript of Pepper&Carrot Episode 27 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 27: Korianders opfindelse

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|KRONINGSDAG AF MAJESTÆTEN
Skrædder|4|False|Om eftermiddagen i Qualicity
Skrædder|5|False|Jeg beder Dem, Prinsesse Koriander…
Koriander|6|False|Kunne De stoppe med at bevæge Dem så meget?
Skrædder|7|False|Ja, ja...
Koriander|8|False|Der er ikke meget tid tilbage. Kroningen starter om tre timer.
Skrift|2|True|...
Skrift|3|False|DRONNING KORIANDER
<hidden>|0|False|Translate “Her Majesty’s Coronation” and “Queen Coriander” on the banner on the building (in the middle). The other text boxes will update automatically (they are clones).
<hidden>|0|False|NOTE FOR TRANSLATORS
<hidden>|0|False|You can strech the scroll and translate “Qualicity” to “City of Qualicity”.
<hidden>|0|False|NOTE FOR TRANSLATORS

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Ser I...
Koriander|2|False|Jeg tænkte igen på sikkerheden...
Koriander|3|True|Så mange unge konger og dronninger er blevet dræbt på deres kroningsdag.
Koriander|4|False|Min familie er ikke blevet skånet.
Koriander|5|True|Og om nogle timer kommer tusindvis af gæster fra hele verden, og jeg får al opmærksomheden.
Pepper|7|True|Bare rolig, Koriander
Koriander|6|False|Jeg er virkelig, virkelig bange!
Pepper|8|True|Vi følger vores plan.
Pepper|9|False|Shichimi og jeg er dine bodyguards.
Shichimi|10|True|Præcis.
Shichimi|11|False|Du burde tage en pause og tænke på noget.
Koriander|12|False|OK.
Koriander|13|False|Hey! Hvad nu hvis jeg viste jer min nyeste opfindelse?
Skrædder|14|False|...
Koriander|15|True|Den er nede på mit værksted.
Koriander|16|False|Følg efter mig.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|False|I kommer snart til at forstå, hvorfor jeg ville bygge den.
Koriander|2|False|Det har noget med mine bekymringer for kroningen at gøre.
Koriander|3|False|Lad mig præsentere min nye opfindelse...
Psykolog-robot|5|False|Hello, world !
Psykolog-robot|6|False|Jeg er jeres PSYKOLOG-ROBOT. Bip, Bip !
Psykolog-robot|7|False|Tag venligst plads på min sofa til en gratis undersøgelse.
Lyd|4|False|Klik!
Lyd|8|False|Tap!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Den sender et elektrisk stød, hver gang man lyver.
Koriander|2|False|Nå, hvad synes I?
Pepper|3|True|Vil I prøve?
Pepper|4|True|Tsk.
Pepper|5|False|Det har jeg ikke brug for.
Lyd|6|True|Jeg har ingen psykologiske problemer|nowhitespace
Pepper|11|False|B
Koriander|12|True|HEY! HVAD ER PROBLEMET MED DIN ROBOT?!
Koriander|13|True|Undskyld!
Koriander|14|False|Jeg kan stadig ikke finde ud at rette den fejl.
Pepper|15|False|Lyve?!
Pepper|16|False|Men jeg har aldrig løjet i hele mit LIV!
Pepper|22|True|DET ER...
Pepper|23|True|IKKE...
Pepper|24|False|SJOVT!!!
Lyd|7|True|Z|nowhitespace
Lyd|8|True|Z|nowhitespace
Lyd|9|True|T|nowhitespace
Lyd|10|False|!|nowhitespace
Lyd|17|True|B
Lyd|18|True|Z|nowhitespace
Lyd|19|True|Z|nowhitespace
Lyd|20|True|T|nowhitespace
Lyd|21|False|!|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Sjovt? Nej, selvfølgelig ikke! Undskyld.
Koriander|2|False|Hi, hi! Du har ret! Slet ikke sjovt!
Pepper|8|True|Ha...
Pepper|9|True|Ha...
Pepper|10|False|Ha.
Koriander|11|True|ÅH NEJ!
Koriander|12|False|Kjolen jeg skulle have på til kroningen er ødelagt! Min skrædder bliver rasende!
Koriander|13|True|Slemme robot!
Koriander|14|False|Jeg retter din fejl med det samme!
Pepper|15|False|VENT!!!
Lyd|3|True|B
Lyd|4|True|Z|nowhitespace
Lyd|5|True|Z|nowhitespace
Lyd|6|True|T|nowhitespace
Lyd|7|False|!|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|KRONING AF MAJESTÆTEN
Lyd|2|False|Jeg har en idé til, hvordan vi kan udnytte fejlen.
Fortæller|3|False|Pling!
Koriander|11|False|Samme aften...
Pepper|12|True|Pepper, du er et geni!
Pepper|13|False|Nej, du er.
Psykolog-robot|14|False|Det er din opfindelse!
Psykolog-robot|15|True|Næste!
Psykolog-robot|16|False|God aften,
Fortæller|17|False|Har De planer om at myrde nogen i aften?
Lyd|6|True|FORTSÆTTES...
Lyd|7|True|B|nowhitespace
Lyd|8|True|Z|nowhitespace
Lyd|9|True|Z|nowhitespace
Lyd|10|False|T|nowhitespace
Skrift|4|True|!
Skrift|5|False|DRONNING KORIANDER

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|I kan også blive tilhængere og få jeres navne skrevet på her!
Pepper|3|True|Pepper & Carrot er frit, gratis, open-source, og sponsoreret af sine læsere.
Pepper|4|False|Denne episode fik støtte af 1060 tilhængere!
Pepper|7|True|Gå på www.peppercarrot.com og få mere information!
Pepper|6|True|Vi er på Patreon, Tipeee, PayPal, Liberapay ... og andre!
Pepper|8|False|Tak!
Pepper|2|True|Vidste I det?
Credits|1|False|31 oktober 2018 Tegning og manuskript: David Revoy Genlæsning i beta-versionen: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Værktøj: Krita 4.2-dev, Inkscape 0.92.3 sur Kubuntu 18.04. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
