# Transcript of Pepper&Carrot Episode 22 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 22: An siostam bhòtaidh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Àrd-bhàillidh Chomona|1|False|’S urrainn dhan fharpais bhuidseachais mhòr againn tòiseachadh mu dheireadh thall!
Sgrìobhte|2|False|Farpais buidseachais

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Àrd-bhàillidh Chomona|1|False|Agus taing dha na h-innleadairean seòlta againn, gabhaidh sibh uile pàirt ann cuideachd!
Àrd-bhàillidh Chomona|2|False|Seo agaibh e, a chàirdean, seallaibh air iongantas an teicneòlais a bheir an luchd-frithealaidh dhuibh!
Àrd-bhàillidh Chomona|3|False|Bheir am putan uaine puing dha cho-fharpaisiche ’s bheir am putan dearg puing air falbh. Is sibhse a nì an co-dhùnadh!
Àrd-bhàillidh Chomona|4|True|An cluinn mi ceist mun choiste?
Àrd-bhàillidh Chomona|5|False|Na gabhaibh dragh dheth! Tha adhbhar dhaibh!
Àrd-bhàillidh Chomona|6|False|Tha uidheam-smachd sònraichte aig gach britheamh agus bheir iad seachad no air falbh ceud phuing leis gach brùthadh!
Peabar|7|False|Abair e!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Àrd-bhàillidh Chomona|5|False|…50,000 Co!
Àrd-bhàillidh Chomona|1|False|Agus an salann sa bhrot – chì sibh na sgòran sa bhad os cionn gach co-fharpaisiche!
Àrd-bhàillidh Chomona|3|False|Leanaidh an triùir a gheibh na sgòran as àirde air adhart dhan chuairt dheireannach!
Àrd-bhàillidh Chomona|4|True|Cumaibh ’nur n-aire gum faigh an tè a bhuannaicheas duais mhòr dhe...
Sgrìobhte|2|False|1337
<hidden>|0|False|Edit this one, all others are linked
An èisteachd|6|False|Clapan

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Curran|2|False|Gnogag
Peabar|3|False|Abair deagh bheachd, nach e, a Churrain?
Peabar|4|True|Ùr-ghnàthach…
Peabar|5|True|Aoibhneil…
Peabar|6|True|Fo smachd an t-sluaigh…
Peabar|7|False|… seo siostam foirfe!
Peabar|8|True|Chan eil sinn an eisimeil air eòlaichean tuilleadh airson breithneachadh dhuinn dè tha math!
Peabar|9|False|Abair adhartas! Abair sàr-linn sa bheil sinn beò!
Àrd-bhàillidh Chomona|10|False|A bheil uidheam aig a h-uile duine?
An èisteachd|11|False|Sgoinneil!
An èisteachd|12|False|Tha!
An èisteachd|13|True|Tha gu
An èisteachd|14|False|dearbh!
An èisteachd|15|False|Aidh!
Àrd-bhàillidh Chomona|16|True|Taghta!
Àrd-bhàillidh Chomona|17|True|Tha an fharpais…
Àrd-bhàillidh Chomona|18|False|air a TÒISEACHADH!!
<hidden>|0|False|Edit this one, all others are linked
An èisteachd|1|False|Clapan

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Àrd-bhàillidh Chomona|1|False|Tòisicheamaid air a’ cho-fharpais le Buidheag!
Fuaim|2|False|Diiiuuuu…
Buidheag|3|False|SYLVESTRIS!
Fuaim|4|False|Beum!
An èisteachd|5|True|Gnogag
An èisteachd|6|True|Gnogag
An èisteachd|7|True|Gnogag
An èisteachd|8|True|Gnogag
An èisteachd|9|True|Gnogag
An èisteachd|10|True|Gnogag
An èisteachd|11|False|Gnogag

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Àrd-bhàillidh Chomona|2|False|Fhuair Buidheag sgòr math! ’S e cuairt Shidimi a th’ ann a-nis!
Sgrìobhte|1|False|5861
Sidimi|4|False|LUX…
Fuaim|5|False|Fii iiiiiss !!|nowhitespace
Sidimi|6|False|MAXIMA!
An èisteachd|7|False|À!!
An èisteachd|8|False|Aoibh!!
An èisteachd|9|False|Mo shùilean!!!
Peabar|12|False|A Churrain... thoir òrdag uaine dhi co-dhiù; ’s e ar caraid a th’ innte...
Curran|13|False|Gnogag
An èisteachd|10|True|Bù!
An èisteachd|11|False|Bùù!
An èisteachd|14|True|Bù!
An èisteachd|15|False|Bùù!
Àrd-bhàillidh Chomona|17|False|À, cha chreid mi gun do chuir an taisbeanadh “dealrach” seo an sluagh o mhothachadh! Seo dhuibh Muirlinn a-nis!
Sgrìobhte|16|False|-42
An èisteachd|18|True|Bùù!
An èisteachd|19|True|Bùù!
An èisteachd|20|True|Bùù!
An èisteachd|21|False|Bù!
<hidden>|0|False|Edit this one, all others are linked
An èisteachd|3|False|Clapan

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Muirlinn|1|False|FUASGLUS KRAKENIS!
Fuaim|2|False|Ubh luluuu!|nowhitespace
Fuaim|3|False|S plaa ish !|nowhitespace
Àrd-bhàillidh Chomona|5|False|Eireachdail! Cumhachdach! Tha Muirlinn air thoiseach càich! A Chostag, do chuairt-sa a-nis!
Sgrìobhte|4|False|6225
Muirlinn ⁊ Sgeach|6|False|Pliut
Costag|8|False|MORTUS REDITUS!
Fuaim|9|False|Gru bhuuu !|nowhitespace
Àrd-bhàillidh Chomona|11|False|Ò, saoil a bheil cnàimhnich a-mach à fasan? Nise, a Chròch chòir!
Sgrìobhte|10|False|2023
<hidden>|0|False|Edit this one, all others are linked
An èisteachd|7|False|Clapan

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cròch|1|False|Grrr! Chan eil dòigh gun dèid nas miosa leam na le Muirlinn! Nì mi mo dhìcheall!
Cròch|2|False|A dh’astar, a Chè!
Fuaim|3|False|Fr rruuuis !|nowhitespace
Fuaim|4|False|Fr rruiiis !|nowhitespace
Fuaim|5|False|Crchh!
Cè|6|False|Miamh!
Cròch|7|False|SPIRALIS
Cròch|8|False|LAS aaaa aaiiiidh!|nowhitespace
Fuaim|9|False|Fr rruuu isss !|nowhitespace
Fuaim|10|False|Slaiiiighd!
Fuaim|11|False|Fis ss !|nowhitespace
Fuaim|12|False|Saidse!
Cròch|13|False|?!!
Fuaim|14|False|Fis sss !|nowhitespace
Fuaim|15|False|Fis ss !|nowhitespace
Cròch|16|False|Aoiiibh! !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cròch|1|False|Iochd... abair tàmailt!
Cròch|3|False|?!
Sgrìobhte|4|True|14
Sgrìobhte|5|False|849
Peabar|6|False|Gnogag
An Rìgh Reòthach|7|True|Gnog
An Rìgh Reòthach|8|True|Gnog
An Rìgh Reòthach|9|False|Gnog
Sgrìobhte|10|True|18
Sgrìobhte|11|False|231
Àrd-bhàillidh Chomona|12|True|Beagan beusachd, a chàirdean!
Àrd-bhàillidh Chomona|13|False|Cha dèid Sidimi no Costag dhan ath-chuairt!
Àrd-bhàillidh Chomona|14|False|Thèid Buidheag, Muirlinn agus Cròch dhan chuairt dheireannach!
<hidden>|0|False|Edit this one, all others are linked
An èisteachd|2|False|Clapan

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Àrd-bhàillidh Chomona|1|False|A chlann-nighean, a bheil sibh deis airson dùbhlan eile?
Àrd-bhàillidh Chomona|2|False|Siuthadaibh!
Sgrìobhte|3|True|1 5|nowhitespace
Sgrìobhte|4|False|703
Sgrìobhte|5|True|19
Sgrìobhte|6|False|863
Sgrìobhte|7|True|1 3|nowhitespace
Sgrìobhte|8|False|614
Fuaim|10|False|Brag!
Peabar|11|False|Thèid mi às àicheadh air na thuirt mi mun t-siostam seo cheana...
An Rìgh Reòthach|12|True|Gnog
An Rìgh Reòthach|13|True|Gnog
An Rìgh Reòthach|14|False|Gnog
Neach-aithris|15|False|- Deireadh na sgeòil -
<hidden>|0|False|Edit this one, all others are linked
An èisteachd|9|False|Clapan

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|05/2017 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Urram|2|False|Piseach air na còmhraidhean: Nicolas Artance, Valvin, Craig Maloney.
Urram|3|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Urram|4|False|Bathar-bog : Krita 3.1.3, Inkscape 0.92.1 air Linux Mint 18.1
Urram|5|False|Ceadachas : Creative Commons Attribution 4.0
Urram|6|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 864 pàtran a thug taic dhan eapasod seo:
Urram|7|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
