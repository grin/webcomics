# Transcript of Pepper&Carrot Episode 32 [ro]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodul 32: Câmpul de bătălie

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Ofi ț er!|nowhitespace
King|2|False|Ai recrutat o vrăjitoare, așa cum am cerut?
Officer|3|True|Da, sire!
Officer|4|False|E lângă domnia ta.
King|5|False|...?
Pepper|6|True|Bună!
Pepper|7|False|E sunt Pepp...
King|8|False|?!!
King|9|True|NERODULE!!!
King|10|True|De ce ai recrutat copilul ăsta?!
King|11|False|Am nevoie de o vrăjitoare războinică!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Pardon!
Pepper|2|True|Sunt o Vrăjitoare Chaosah adevărată.
Pepper|3|False|Am chiar și o diplomă pe care scrie...
Writing|4|True|Certificat
Writing|5|True|de la
Writing|6|False|Chaosah
Writing|8|True|Cayenne
Writing|9|False|Cumin
Writing|7|True|T h yme|nowhitespace
Writing|10|False|~ pentru Pepper ~
King|11|False|TĂCERE!
King|12|True|Nu îmi trebuie copii în bătălia asta.
King|13|False|Du-te acasă și joacă-te cu păpușile.
Sound|14|False|Slap!
Army|15|True|HAHA HA HA!
Army|16|True|HAHA HA HA!
Army|17|True|HAHA HA HA!
Army|18|False|HA HA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Nu-mi vine să cred!
Pepper|2|False|Am învățat ani întregi, dar nimeni nu mă ia în serios pentru că...
Pepper|3|False|...Nu arăt ca cineva suficient de experimentat!
Sound|4|False|PUF ! !|nowhitespace
Pepper|5|False|CARROT !|nowhitespace
Sound|6|False|PAF ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Serios, Carrot?
Pepper|2|True|Sper că ce mănânci a meritat efortul.
Pepper|3|False|Arăți complet hidoasă...
Pepper|4|False|...Arăți...
Pepper|5|True|Aparența!
Pepper|6|False|Desigur!
Sound|7|False|Crac !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HEI!
Pepper|2|False|Am auzit că sunteți în căutarea unei VRĂJITOARE ADEVĂRATE ?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Dacă mă gândesc mai bine, cheam-o pe copila aia înapoi.
King|2|False|Asta cred că o să coste prea mult.
Writing|3|False|VA URMA…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Și tu poți deveni patron al Pepper&Carrot și să ai numele trecut aici !
Pepper|3|True|Pepper&Carrot este complet gratuit, open-source și sponzorizat prin patronajul cititorilor săi.
Pepper|4|False|Pentru acest episod, mulțumiri celor 1121 de patroni!
Pepper|7|True|Intră pe www.peppercarrot.com pentru detalii!
Pepper|6|True|Suntem pe Patreon, Tipeee, PayPal, Liberapay ...și altele!
Pepper|8|False|Mulțumim!
Pepper|2|True|Știai că?
Credits|1|False|31 Martie, 2020 Artă & scenariu: David Revoy. Cititori beta: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Versiune limba română Traducere: Florin Șandru Bazat pe universul Hereva Creator: David Revoy. Administrator: Craig Maloney. Scriitori: Craig Maloney, Nartance, Scribblemaniac, Valvin. Corectori: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2.9-beta, Inkscape 0.92.3 on Kubuntu 19.10. Licență: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
