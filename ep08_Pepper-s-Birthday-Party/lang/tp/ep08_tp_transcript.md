# Transcript of Pepper&Carrot Episode 08 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka tu wan: jan Pepa li sike sin e suno

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|tenpo suno musi mi li kama. taso jan pona ala la, mi ken musi ala e ni...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|a, sina wile e ni: utala pini pi pali telo* la, jan ante pi wawa nasa li ken kama
jan Pepa|3|False|pona a, soweli Kawa o!
sitelen|4|False|o kama
sitelen|2|False|* o lukin e lipu nanpa luka wan: utala pi pali telo

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|pini a ...
jan Pepa|2|False|... ale li pona! tenpo ni la mi awen tawa kama ona.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|2|False|... ona li kama ...
jan Pepa|1|True|taso mi pilin e ni... mi sona e ni...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|tan seme la ona li pali e ni tawa mi ?!! tawa
jan Pepa|2|True|mi
sitelen|4|False|toki kama pi kon ike pi nasin PAKALA nanpa 1
jan Pepa|5|False|... ona li wile ala pona. tan ni la ...
kalama|6|False|L L L O OJ E|nowhitespace
jan Pepa|3|False|!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sewi ike|1|False|šina lawà e kon ikë pi ņasin Pakalāa!
sewi ike|2|True|sina wile e ûtala táwa
sewi ike|3|False|jan šeme ? ...
sitelen toki|4|False|lipu nanpa luka tu wan: jan Pepa li sike sin e suno
sitelen toki|5|False|PINI
mama|6|False|tenpo June 2015 - www.peppercarrot.com - musi sitelen en toki li tan jan David Revoy - ante toki li tan jan Ret Samys

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 354 li pana e mani la, lipu ni li lon:
mama|4|False|https://www.patreon.com/davidrevoy
mama|3|True|sina ken pana kin e pona tawa jan Pepa&soweli Kawa tawa lipu kama:
mama|7|False|ilo : lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Free/Libre li ilo Krita, li ilo Inkscape lon ilo GNU/Linux
mama|6|False|nasin Open-source : sitelen mama suli ale li lon lipu mama, li jo e sitelen mute e sitelen linja.
mama|5|False|nasin lawa : Creative Commons Attribution sina ken ante, li ken pana, li ken esun, li ken pali ante...
mama|2|False|Глеб Бузало - 獨孤欣 & 獨弧悦 - Adam - Addison Lewis - A Distinguished Robot - Adrian Lord - Ahmad Ali - Aina Reich - al - Alandran Alan Hardman - Albert Westra - Alcide - Alex - Alexander Bülow Tomassen - AlexanderKennedy - Alexander Sopicki - Alexandra Jordan Alex Bradaric - Alexey Golubev - Alex Kotenko - Alex Lusco - Alex Silver - Alex V - Alfredo - Ali Poulton (Aunty Pol) - Allan Zieser - Alok Baikadi Andreas Rieger - Andreas Ulmer - Andrej Kwadrin - Andrew - Andrew Godfrey - Andrey Alekseenko - Andy Gelme - Anna Orlova - anonymous Antan Karmola - Anthony Edlin - Antoine - Antonio Mendoza - Antonio Parisi - Ardash Crowfoot - Arjun Chennu - Arne Brix - Arnulf - Arturo J. Pérez Axel Bordelon - Axel Philipsenburg - Ayaskull - barbix - BataMoth - Bela Bargel - Ben Evans - Bernd - Bernhard Saumweber - Betsy Luntao Birger Tuer Thorvaldsen - blacksheep33512 - Boonsak Watanavisit - Boris Fauret - Boudewijn Rempt - BoxBoy - Brent Houghton - Brett Brett Smith - Brian Behnke - Bryan Butler - Bryan Rosander - BS - Bui Dang Hai Trieu - BXS - carlos levischi - Cedric Wohlleber - Charles Charlotte Lacombe-bar - Chris - Chris Radcliff - Chris Sakkas - Christian Gruenwaldner - Christophe Carré - Christopher Bates Christopher Rodriguez - Clara Dexter - codl - Colby Driedger - Conway Scott Smith - Coppin Olivier - Cuthbert Williams - Cyol Cyrille Largillier - Cyril Paciullo - Damien - Daniel - Daniel Björkman - Daniel Lynn - Danny Grimm - Dan Stolyarov - David - David Kerdudo David Tang - Davi Na - Davis Aites - Dawn Blair - DecMoon - Dezponia Veil - DiCola Jamn - Dmitry - Donald Hayward - Douglas Oliveira Pessoa Doug Moen - Duke - Eitan Goldshtrom - Ejner Fergo - Enrico Billich - Enrique Lopez - epsilon - Eric Schulz - Erik Moeller Esteban Manchado Velázquez - Faolan Grady - Fen Yun Fat - Francois Schnell - francou - freecultureftw - Garret Patterson - Gary Thomas Ginny Hendricks - GreenAngel5 - Grigory Petrov - G. S. Davis - Guillaume - Gustav Strömbom - Happy Mimic - Helmar Suschka - Henning Döscher Henry Ståhle - HobbyThor - Igor - Ilyas - Irina Rempt - Ivan Korotkov - Jacob - James Frazier - Jamie Sutherland - Janusz - Jared Tritsch Jason - JDB - Jean-Baptiste Hebbrecht - Jean-Gabriel Loquet - Jeffrey Schneider - Jessey Wright - Jessica Gadling - Jhonny Rosa - Jim Jim Street - Jiska - Joao Luiz - Joerg Raidt - Joern Konopka - joe rutledge - John - John - John Gholson - John Urquhart Ferguson Jónatan Nilsson - Jonathan Leroy - Jonathan Ringstad - Jon Brake - Jorge Bernal - Joseph Bowman - Josh Cavalier - Juju Mendivil - Julia Velkova Julio Avila - Justus Kat - Kailyce - Kai-Ting (Danil) Ko - Kate - Kathryn Wuerstl - Ken Mingyuan Xia - Kevin Estalella - Kevin Trévien - Kingsquee Kroet - Kurain - La Plume - Lars Ivar Igesund - Lenod - Levi Kornelsen - Liang - Liselle - Lise-Lotte Pesonen - Lloyd Ash Pyne - Lorentz Grip Lorenzo Leonini - Louis Yung - Luc Stepniewski - Luke Hochrein - Magnus Kronnäs - Mahwiii - Manuel - Manu Järvinen - Marc et Rick Marco Sousa - marcus - Martin Owens - Mary Brownlee - Masked Admirer - Matthew Reynolds - Matt Lichtenwalner - mefflin ross bullis-bates Michael - Michael F. SChönitzer - Michael Gill - Michael Pureka - Michelle Pereira Garcia - Mike Mosher - Miriam Varón - Miroslav - mjkj Moritz Fuchs - Muriah Summer - Nazhif - Nicholas DeLateur - Nicholas Terranova - Nicki Aya - Nicola Angel - Nicolae Berbece - Nicole Heersema Nielas Sinclair - NinjaKnight Comics - Noble Hays - Noelia Calles Marcos - Nora Czaykowski - No Reward - Nyx - Oleg Schelykalnov Olga Bikmullina - Olivier Amrein - Olivier Brun - Olivier De Rop - Olivier Gavrois - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. Pat David - Patrick Dezothez - Patrick Gamblin - Paul - Paul - Pavel Semenov - Pet0r - Peter - Peter Moonen - Petr Vlašic Philippe Jean Edward Bateman - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Praveen Bhamidipati - Pyves & Ran - Raghavendra Kamath Rajul Gupta - Ramel Hill - Raymond Fullon - Ray Powell - Rebecca Morris - Reorx Meng - Ret Samys - Reuben Tracey - Ricardo Muggli - rictic RJ van der Weide - Roberto Zaghis - Robin Moussu - Roman Burdun - Rose “flash” Flashis - Rumiko Hoshino - Rustin Simons - Sally Bridgewater Sami T - Samuel Mitson - Scott Petrovic - Sean Adams - Sebastien - Sevag Bakalian - ShadowMist - shafak - Shawn Meyer - Simon Forster Simon Isenberg - Sonja Reimann-Klieber - Sonny W. - Soriac - Stanislav German-Evtushenko - Stanislav Vodetskyi - Stephan Theelke Stephen Bates - Stephen Smoogen - Steven Bennett - Stuart Dickson - surt - Taedirk - TamaskanLEM - tar8156 - Tas Kartas - Terry Hancock TheFaico - thibhul - Thomas Citharel - Thomas Courbon - Thomas Schwery - Thor Galle - Thornae - Tim Burbank - Tim J. - Tomas Hajek Tom Savage - Travis Humble - tree - Tristy - Tyson Tan - uglyheroes - Urm - usfreitas - Vera Vukovic - Victoria - Victoria White Vladislav Kurdyukov - Vlad Tomash - WakoTabacco - Wander - Westen Curry - Wilhelmine Faust - Xavier Claude - Yalyn Vinkindo - Yaroslav Yasmin - Zeni Pong - Źmicier Kušnaroŭ - zubr kabbi.
