# Transcript of Pepper&Carrot Episode 06 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 6 : Le concours de potion

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh zut, je me suis encore couchée sans fermer la fenêtre...
Pepper|2|True|...mais quel vent!
Pepper|3|False|... et pourquoi je vois Komona par la fenêtre?
Pepper|4|False|KOMONA!
Pepper|5|False|Le concours de Potion!
Pepper|6|True|J'ai... J'ai dû m'endormir sans le faire exprès !
Pepper|9|True|... mais?
Pepper|10|False|Où je suis là ?!?
Oiseau|12|False|N?|nowhitespace
Oiseau|11|True|coi|nowhitespace
Pepper|7|False|*
Note|8|False|* voir episode 4 : Coup de génie

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! T'es trop mignon d'avoir pensé à m'emmener au concours !
Pepper|3|False|Ma-gni-fi-que !
Pepper|4|True|Tu as même pensé à prendre une potion, mes vêtements, et mon chapeau...
Pepper|5|False|.. voyons voir quelle potion tu as pris ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|QUOI ?!!
Maire de Komona|3|False|Je déclare, en tant que Maire de Komona, le concours de potion ouvert !
Maire de Komona|4|False|Notre ville est très contente d'accueillir pas moins de quatre sorcières pour cette première.
Maire de Komona|5|True|Merci d'applaudir
Maire de Komona|6|False|bien fort :
Écriture|2|False|Concours de Potion de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Maire de Komona|1|True|Venue du grand pays de l'Union des Technologistes, c'est un honneur d'accueillir la ravissante et ingénieuse
Maire de Komona|3|True|... sans oublier notre fille du pays, la sorcière elle même de Komona
Maire de Komona|5|True|... notre troisième participante nous vient du pays des lunes couchantes
Maire de Komona|7|True|... enfin, notre dernière participante qui nous vient de la forêt de Bout-un-Cureuil
Maire de Komona|2|False|Coriandre !
Maire de Komona|4|False|Safran !
Maire de Komona|6|False|Shichimi !
Maire de Komona|8|False|Pepper !
Maire de Komona|9|True|Que le concours commence !
Maire de Komona|10|False|Le vote se fera à l'applaudimètre !
Maire de Komona|11|False|Et tout d'abord, place à la démonstration de Coriandre
Coriandre|12|False|Mesdames et Messieurs...
Coriandre|13|True|... plus besoin d'avoir peur de la mort grâce à ...
Coriandre|14|True|.. ma Potion de
Coriandre|15|False|ZOMBIFICATION !
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Maire de Komona|1|False|FANTASTIQUE !
Maire de Komona|2|False|Coriandre défie la mort elle-même avec cette potion mi-ra-cu-leu-se !
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|True|Clap
Audience|17|False|Clap
Safran|19|True|car voici
Safran|18|False|... mais enfin, gardez vos applaudissements, peuple de Komona !
Safran|22|True|La vraie potion que vous attendez tous : celle qui peut impressionner tous vos voisins ...
Safran|23|False|... les rendre jaloux!
Safran|20|True|MA
Safran|26|False|BOURGEOISIE !
Safran|25|True|... potion de
Safran|24|True|... tout ceci est désormais possible par l'application d'une simple goutte de ma ...
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|True|Clap
Audience|42|False|Clap
Audience|41|True|Clap
Maire de Komona|44|False|cette potion pourrait rendre le tout Komona riche!
Maire de Komona|43|True|Fantastique! Incroyable !
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Audience|3|True|Clap
Audience|27|True|Clap
Maire de Komona|45|False|Vos applaudissements ne s'y trompent pas; Coriandre est déjà éliminée.
Safran|21|False|Potion

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Maire de Komona|1|False|Cela va être à présent difficile de rivaliser pour Shichimi !
Shichimi|4|True|NON !
Shichimi|5|True|je ne peux pas, trop dangereux
Shichimi|6|False|PARDON !
Maire de Komona|3|False|... allons Shichimi, tout le monde vous attend
Maire de Komona|7|False|Il semblerait, Mesdames et Messieurs que Shichimi déclare forfait...
Safran|8|False|Donne moi ça !
Safran|9|False|... et puis arrête de faire ta timide et ne gâche pas le spectacle
Safran|10|False|Tout le monde sait déjà que j'ai gagné le concours alors quoi que ta potion fasse ...
Shichimi|11|False|!!!
Son|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|MONSTRE GÉANT !
Shichimi|2|False|Je... je ne savais pas qu'il fallait faire une démonstration
Shichimi|13|True|ATTENTION!!!
Shichimi|14|True|C'est une potion de

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Oiseau|1|False|CRi CRi CRiiii ii ii ii ii ii ii iii i ii|nowhitespace
Son|2|False|BAM!
Pepper|3|True|... heu cool !
Pepper|5|False|... ma potion devrait au moins avoir le mérite de vous faire rire car....
Pepper|4|False|c'est donc à mon tour maintenant ?
Maire de Komona|6|True|Fuis idiote!
Maire de Komona|7|False|le concours est fini ! ... sauve ta peau!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...comme d'habitude tout le monde part au moment où c'est à nous
Pepper|1|True|et voilà :
Pepper|4|True|En tout cas j'ai une petite idée de ce que l'on va pouvoir faire avec ta «potion» Carrot
Pepper|5|False|...remettons de l'ordre ici et rentrons !
Pepper|7|True|Toi le
Pepper|8|False|Piou-piou-géant-zombie-bourgeois !
Pepper|10|False|Ça te plairait de tester la dernière potion ?...
Pepper|11|False|... pas vraiment hein ?
Pepper|6|False|HÉ !
Son|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oui, lis bien l'étiquette...
Pepper|2|False|.. je n'hésiterai pas à t'en verser dessus si tu dégages pas tout de suite de Komona !
Maire de Komona|3|True|Parce qu'elle a sauvé notre ville d'un désastre,
Maire de Komona|4|False|nous remettons la première place à Pepper pour sa Potion de .. ??!!
Pepper|7|False|... heu... en fait, c'est pas vraiment une potion ; ce sont les échantillons d'urine de mon chat pour sa visite médicale !
Pepper|6|True|... Haha! oui ...
Pepper|8|False|... pas de démo hein ?...
Narrateur|9|False|Episode 6 : Le concours de potion
Narrateur|10|False|FIN
Écriture|5|False|50 000 Ko
Crédits|11|False|March 2015 - Dessin et Scénario : David Revoy , corrections : Aurélien Gâteau

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat des lecteurs. Pour cet épisode, merci aux 245 Mécènes :
Crédits|4|False|https://www.patreon.com/davidrevoy
Crédits|3|False|Vous aussi, devenez mécène de Pepper&Carrot pour le prochain épisode :
Crédits|7|False|Tools : Cet épisode a été dessiné a 100% avec des logiciels libres Krita sur Linux Mint
Crédits|6|False|Open-source : toutes les sources, polices d'écritures, fichiers avec calques sont disponibles sur le site officiel au téléchargement.
Crédits|5|False|License : Creative Commons Attribution vous pouvez modifier, repartager, vendre, etc...
Crédits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
