# Transcript of Pepper&Carrot Episode 19 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 19: Pollution

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|I'm going in.
Cayenne|2|True|No use in watching you dig...
Cayenne|3|False|Bury all these failed potions for me and get in as soon as you can to rest.
Cayenne|4|False|Seeing you succeed tomorrow is probably just wishful thinking, but who knows!
Pepper|5|True|Alright, I'll hurry!
Pepper|6|True|But, by the way, what's this thing about always burying everything ?
Pepper|7|False|Wouldn't it be better if we...
Cayenne|8|False|If WHAT ?

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|labels in the garden :-)
Pepper|1|True|Uhm...
Pepper|2|True|Well, of course, I'm no expert...
Pepper|3|True|But the vegetable patch this year is really weird
Pepper|4|False|and that goes for a lot of the other plants around the house too.
Writing|5|False|Tomatoes
Writing|6|False|Aubergines
Pepper|7|False|Same with the ants, they're really up to some strange stuff.
Pepper|8|True|So, anyway...
Pepper|9|False|I thought to myself that maybe we have a bit of a problem with pollution and should think about cleaning up a bit...
Cayenne|10|True|Listen, Miss " I-Ruin-All-My-Potions ",
Cayenne|11|False|your Hippiah costume is no doubt going to your head.
Cayenne|12|True|In Chaosah, we bury our mistakes!
Cayenne|13|True|It's been our tradition since the beginning of time and I don't give a hoot what MOTHER NATURE thinks about it!
Cayenne|14|False|So you shut it and you DIG !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|we bury our mistakes
Pepper|2|False|tradition
Pepper|3|False|since the beginning of time
Pepper|4|False|OF COURSE!
Carrot|5|False|Zzzz
Pepper|6|False|Come on, faster, Carrot!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Diary label on book
Writing|6|False|Cayenne's Private Journal
Pepper|1|False|"Oh, handsome warrior, blond and golden-locked!"
Pepper|2|False|"...You who haunt my chaos!"
Pepper|3|False|"...You are the entropy of my auroras."
Pepper|4|True|Nice poetry, mistress Cayenne!
Pepper|5|False|It's crazy what you can learn about witches who bury all of their mistakes!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|We have all had our failures.
Cayenne|2|True|I am no longer the Cayenne who wrote that.
Cayenne|3|False|That journal was buried for a very specific reason.
Pepper|4|False|...
Pepper|5|True|OK!
Pepper|6|False|But what do you say to this then?
Writing|7|False|CHAOSAH SUTRA by Thyme
Thyme|8|False|It's simply an error of my youth, and in any case isn't appropriate for your age!
Pepper|9|True|Hmm...
Pepper|10|False|I see...
Pepper|11|False|You really can't be embarrassed by any of this...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|But the environment ! Nature !
Pepper|2|False|We can't keep polluting everything like this without suffering serious consequences!!!
Cayenne|3|True|It hasn't caused us any problems so far!
Cayenne|4|True|We are witches of Chaosah! And our problems get buried
Cayenne|5|True|deeply!
Cayenne|6|False|We will not debate traditions!
Cumin|7|True|Oh, look what I've just found!
Cumin|8|False|I can't believe it!
Cumin|9|False|How did this get down here?
Cumin|10|False|She probably needs a little tuning, but still strums alright.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Label of materials to translate
Cumin|1|False|How did it go again? Cha~Cha Cha, Chaooosah!
Cumin|2|True|Haha, I've forgotten the lyrics!
Cumin|3|False|Maybe my song book is here somewhere too...
Cayenne|4|True|...so, we're all in agreement. Update to the rules of Chaosah:
Cayenne|5|True|From now on, we
Cayenne|6|True|sort,
Cayenne|7|True|crush
Cayenne|8|True|and recycle everything!
Cayenne|9|False|EVERYTHING!!
Writing|10|False|glass
Writing|11|False|metal
Narrator|12|False|- FIN -
Credits|13|False|09/2016 - www.peppercarrot.com - Art & Scenario : David Revoy
Credits|14|False|Script doctor: Craig Maloney. Proofreading and assistance with dialog: Valvin, Seblediacre and Alex Gryson. Inspiration: "The book of secrets" by Juan José Segura
Credits|15|False|Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|16|False|License : Creative Commons Attribution 4.0, Software: Krita 3.0.1, Inkscape 0.91 on Arch Linux XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 755 Patrons:
Credits|2|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
