# Transcript of Pepper&Carrot Episode 19 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 19: An truailleadh

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cìob|1|True|Tha mi a’ dol a-steach.
Cìob|2|True|Chan fhiach coimhead ort a’ cladhadh ...
Cìob|3|False|Adhlaic dhomh na deochan seo uile a dh’fhairtlich ort is thig a-steach cho luath ’s as urrainn dhut ach am faigh thu beagan fois.
Cìob|4|False|Ar leam chan e ach miann ro-dhòchasach a th’ ann gun rachadh leat a-màireach, ach cò aig’ a tha fios!
Peabar|5|True|Tha mi agaibh, greas orm!
Peabar|6|True|Ach eadar dà sgeul, dè as adhbhar gun adhlaic sinn a h-uile càil ?
Peabar|7|False|Nach biodh e na b’ fheàrr nan ...
Cìob|8|False|B’ ÀILL leat?

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|labels in the garden :-)
Peabar|1|True|An-dà...
Peabar|2|True|Uill, chan e eòlaiche a th’ annam idir...
Peabar|3|True|Ach tha na glasraich ann an trum neònach am bliadhna
Peabar|4|False|agus tha dòigh an-àbhaisteach air tòrr lus eile timcheall air an taigh.
Sgrìobhte|5|False|Tomàto
Sgrìobhte|6|False|Aubergine
Peabar|7|False|Mar a tha air na seanganan, dè fo na gealaichean a tha iadsan ris?
Peabar|8|True|Mar sin dheth ...
Peabar|9|False|Cha chreid mi nach eil duilgheadas beag bìodach le truailleadh againn ’s bu chòir dhuinn beachdachadh air beagan glanaidh ...
Cìob|10|True|A nighean-uasal “ Mhill-mi-deoch-sam-bith-a-riamh ”,
Cìob|11|False|nach ann ortsa a tha an ceann mòr is fasan na Clann-fhlùrachd ort.
Cìob|12|True|San Dubh-choimeasgachd, ’s ann gun adhlaic sinn ar mearachdan!!
Cìob|13|True|Bu sin ar dualchas o chian nan cian agus chan eil diù a’ choin agam air dè shaoileas MATHAIR NÀDAIR mu dhèidhinn!
Cìob|14|False|Dùin do ghob is CLADHAICH !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|’s ann gun adhlaic sinn ar mearachdan
Peabar|2|False|sin ar dualchas
Peabar|3|False|o chian nan cian
Peabar|4|False|THA E AGAM!
Curran|5|False|Srann
Peabar|6|False|Siuthad nas luaithe a Churrain!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Diary label on book
Sgrìobhte|6|False|Leabhar-latha prìobhaideach Cìoba
Peabar|1|False|“A ghille dhuinn uasail an fhuilt dhualaich!”
Peabar|2|False|"... mo bhruadair Dhubh-choimeasgaich!"
Peabar|3|False|“... Is tusa fear-clis dubh m’ eantropaidh.”
Peabar|4|True|Abair bàrdachd, a bhean-uasal Chìob!
Peabar|5|False|Nach iongantach dè ghabhas ionnsachadh mu bhana-bhuidsichean a dh’adhlaiceas na mearachdan aca uile!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cìob|1|True|Chan eil tè beò nach do dh’fhairtlich oirre a-riamh.
Cìob|2|True|Cha mhise a’ Chìob a sgrìobh sin tuilleadh.
Cìob|3|False|Cha deach an leabhar-latha seo adhlacadh gun adhbhar.
Peabar|4|False|...
Peabar|5|True|Tha mi a’ tuigsinn!
Peabar|6|False|Ach dè ur beachd air an leabhar seo ma-thà
Sgrìobhte|7|False|COIMEASGA SUTRA le Tìom
Tìom|8|False|Chan e seo ach mearachd m’ òige agus chan eil e iomchaidh dha d’ aois air dòigh sam bith!
Peabar|9|True|Cò shaoileadh...
Peabar|10|False|Tha mi agaibh...
Peabar|11|False|Cha chuir gin sam bith dheth nàire oirbh idir...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Ach an àrainneachd ! An nàdar !
Peabar|2|False|Chan urrainn dhuinn cumail oirnn ris an truailleadh oir ’s daor a’ cheannachd a bhios againn air!!
Cìob|3|True|Cha do dh’adhbh-raich e trioblaid sam bith oirnn gu ruige seo!
Cìob|4|True|’S e bana-bhuidsichean na Dubh-choimeasgachd a th’ annainn ’s thèid ar duilgheadasan adhlacadh
Cìob|5|True|gu domhainn!
Cìob|6|False|Na biodh deasbad againn mun dualchas!
Carabhaidh|7|True|Saoil dè lorg mi!
Carabhaidh|8|False|Cha chreid mi e!
Carabhaidh|9|False|Gu duda a dh’fhàg e shìos an-seo?
Carabhaidh|10|False|Bidh e feumach air beagan gleusaidh ach tha e ag obair fhathast.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Label of materials to translate
Carabhaidh|1|False|Dè bh’ ann a-rithist? “Hogainn dùbh-ò-choimeasgachd?”
Carabhaidh|2|True|Haha, dhìochuimhnich mi na faclan!
Carabhaidh|3|False|Saoil a bheil leabhar nan òran agam am badeigin cuideachd...
Cìob|4|True|...nise, tha sinn uile air aontachadh. Ùrachadh air riaghailtean na Dubh-choimeasgachd:
Cìob|5|True|O seo a-mach, nì sinn
Cìob|6|True|seòrsachadh,
Cìob|7|True|pronnadh ’s
Cìob|8|True|ath-chuairteachadh air a h-uile càil!
Cìob|9|False|A h-UILE CÀIL!!
Sgrìobhte|10|False|glainne
Sgrìobhte|11|False|meatailt
Neach-aithris|12|False|- Deireadh na sgeòil -
Urram|13|False|09/2016 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Urram|14|False|Dotair sgriobtaichean: Craig Maloney. Ceartachadh is taic leis na còmhraidhean: Valvin, Seblediacre ’s Alex Gryson. Brosnachadh: "The book of secrets" le Juan José Segura
Urram|15|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Urram|16|False|Ceadachas : Creative Commons Attribution 4.0, Bathar-bog: Krita 3.0.1, Inkscape 0.91 air Arch Linux XFCE

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 755 pàtran a thug taic dhan eapasod seo:
Urram|2|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
