# Transcript of Pepper&Carrot Episode 10 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 10: Sommer-Special

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|- ENDE -
Impressum|2|False|08/2015 - Grafik & Handlung: David Revoy Übersetzung: Alexandra Jordan & Helmar Suschka

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 422 Förderer:
Impressum|3|False|https://www.patreon.com/davidrevoy
Impressum|2|True|Du kannst die nächste Episode von Pepper&Carrot hier unterstützen:
Impressum|4|False|Lizenz : Creative Commons Namensnennung 4.0 Quelldaten : verfügbar auf www.peppercarrot.com Software : Diese Episode wurde zu 100% mit freier Software erstellt Krita 2.9.6, Inkscape 0.91 auf Linux Mint 17
