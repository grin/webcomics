# Transcript of Pepper&Carrot Episode 34 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 34 : L'Adoubement de Shichimi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|False|La même nuit...
Hibiscus|2|False|...et ainsi, ce soir, nous t'accueillons, Shichimi, en tant que plus jeune Chevalier de Ah.
Coriandre|3|False|Toujours aucun signe de Pepper ?
Safran|4|False|Toujours pas.
Safran|5|False|Elle ferait mieux de se dépêcher, ou elle va louper le discours de Shichimi.
Shichimi|6|False|Merci.
Shichimi|7|False|Je voudrais...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Zio oOOOOO|nowhitespace
Son|2|False|Zioo O O O OO|nowhitespace
Pepper|3|True|Chaud devant !
Pepper|4|True|Attention !
Pepper|5|False|ATTENTION !!!
Son|6|False|CR A S H !|nowhitespace
Son|7|False|Oups !
Son|8|False|Tout le monde va bien ? Rien de cassé ?
Shichimi|9|False|Pepper !
Pepper|10|True|Salut, Shichimi !
Pepper|11|True|Désolée pour l'entrée fracassante, et pour le retard !
Pepper|12|False|J'ai couru toute la journée, mais c'est une longue histoire.
<hidden>|13|False|Dear translator: if editing the soundFX is difficult, I made a special documentation here: https://www.peppercarrot.com/en/static14/documentation&page=055_Sound-Effect_translation

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hibiscus|1|False|C'est une de tes invités ?
Shichimi|2|False|Oui. C'est mon amie Pepper. Tout va bien.
Pepper|3|True|Carrot, ça va aller ?
Pepper|4|False|Désolée, je ne maitrise pas encore très bien l'atterrissage en hypervitesse.
Pepper|5|True|Et encore désolée pour le dérangement,
Pepper|6|False|et aussi pour ma tenue....
Shichimi|7|False|Hi, hi !
Wasabi|8|True|Shichimi,
Wasabi|9|False|cette jeune sorcière qui vient d'arriver, est-elle vraiment une de tes amies ?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Oui, votre altesse.
Shichimi|2|False|Elle s'appelle Pepper, de l'école de Chaosah.
Wasabi|3|True|Sa présence ici pollue la nature sacrée de notre école.
Wasabi|4|False|Mets-la hors de ma vue, immédiatement.
Shichimi|5|True|Mais...
Shichimi|6|False|Maître Wasabi...
Wasabi|7|True|Mais quoi ?
Wasabi|8|False|Préfères-tu qu'on te bannisse de notre école ?
Shichimi|9|False|! ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Désolée Pepper, mais tu dois partir.
Shichimi|2|False|Maintenant.
Pepper|3|True|Hein ?
Pepper|4|False|Hop, hop, hop, une minute ! Je suis sûre qu'il y a un malentendu.
Shichimi|5|False|S'il te plaît, Pepper, ne rends pas les choses plus difficiles.
Pepper|6|False|Hé ! Toi, là, sur le trône ! Si t'as un problème avec moi, descends et viens me le dire toi-même !
Wasabi|7|False|Pfff...
Wasabi|8|True|Shichimi, tu as dix secondes...
Wasabi|9|True|neuf...
Wasabi|10|True|huit...
Wasabi|11|False|sept...
Shichimi|12|False|ÇA SUFFIT, PEPPER ! VA T'EN !!!
Son|13|False|SHRRIiii !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Shichimi, s'il te plaît, calme-t...
Son|2|False|B ADou M!|nowhitespace
Shichimi|3|True|VA T'EN !!!
Shichimi|4|True|VA T'EN !!!
Shichimi|5|False|VA T'EN !!!
Son|6|False|C R E E E E E ! !!|nowhitespace
Pepper|7|True|Outch !
Pepper|8|False|Hé ! Ce n'est... p-PAS... ouille... sympa !
Coriandre|9|False|SHICHIMI ! PEPPER ! ARRÊTEZ, S'IL VOUS PLAÎT !
Safran|10|False|Attends.
Wasabi|11|False|Hmm !
Pepper|12|True|Grrr !
Pepper|13|False|OK, tu l'auras cherché !
Son|14|False|B R Z OO !!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CURSUS CANCELLARE MAXIMUS !!!
Son|2|False|S H K L AK !|nowhitespace
Pepper|3|False|Outch !
Son|4|False|P AF !!|nowhitespace
Shichimi|5|True|Même ton meilleur sort d'annulation n'a aucun effet sur moi !
Shichimi|6|True|Abandonne, Pepper, et va t'en !
Shichimi|7|False|Ne m'oblige pas à te faire plus de mal !
Pepper|8|False|Oh mon sort d'annulation a très bien marché, sauf que ce n'était pas toi, la cible.
Shichimi|9|True|Hein ?
Shichimi|10|False|Que veux-tu dire ?!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|?!!
Pepper|2|True|Ma cible, c'était elle !
Pepper|3|False|J'ai simplement annulé son Sort de Jouvence , qui lui donne une apparence plus jeune.
Pepper|4|True|Je l'ai remarqué dès mon arrivée.
Pepper|5|False|Et ça, ce n'est qu'un aperçu de ce que tu mérites pour avoir forcé Shichimi à se battre contre moi !
Wasabi|6|True|INSOLENTE !
Wasabi|7|True|Comment oses-tu,
Wasabi|8|False|et devant toute mon école !
Pepper|9|True|Estime-toi heureuse !
Pepper|10|False|Si j'avais plus de Réa, j'aurais été bien plus loin !

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|DR Z OO !!|nowhitespace
Wasabi|2|True|Je vois... tu as atteint le niveau de tes prédécesseurs bien plus tôt que je ne l'avais prévu...
Wasabi|3|False|Cela précipite mes plans, mais c'est une bonne nouvelle.
Pepper|4|True|Tes plans ?
Pepper|5|True|Donc ce n'était qu'un test, et tout ça n'a aucun rapport avec mon retard ?
Pepper|6|False|Tu es vraiment tordue !
Wasabi|7|True|Ha...
Wasabi|8|False|ha.
Wasabi|9|True|QU'EST-CE QUE VOUS REGARDEZ, VOUS AUTRES ?
Wasabi|10|False|JE VIENS DE ME FAIRE ATTAQUER, ET VOUS RESTEZ PLANTÉS LÀ ?! ATTRAPEZ-LA !!!
Wasabi|11|False|JE LA VEUX VIVANTE !
Wasabi|12|False|ATTRAPEZ-LA !!!
Pepper|13|False|Shichimi, on en reparlera plus tard !
Pepper|14|True|Désolée Carrot, mais on va devoir partir d'ici en hypervitesse une fois de plus.
Pepper|15|False|Accroche-toi !
Son|16|False|Tap !
Son|17|False|Tap !
Son|18|False|Ziioo OO !!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|? !
Wasabi|2|False|ATTRAPEZ-LA !!!
Pepper|3|False|Zut.
Safran|4|False|Pepper, prends mon balai !
Son|5|False|Fizzz !
Pepper|6|True|Oh, super !
Pepper|7|False|Merci, Safran !
Son|8|False|Toc !
Son|9|False|zioo O O O O!|nowhitespace
Narrateur|10|False|À SUIVRE...

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Le 31 Mars 2021 Art & scénario : David Revoy. Lecteurs de la version bêta : Arlo James Barnes, Carotte, Craig Maloney, Efrat b, GunChleoc, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Parnikkapore, Valvin. Traduction française : Nicolas Artance, David Revoy, Valvin, Quetzal2 Remerciement spécial : à Nicolas Artance pour l'exploration du personnage de Wasabi dans ses nouvelles de fan-fiction. La façon dont il l'a imaginée a eu un grand impact sur ma façon dont je l'ai dépeinte dans cet épisode. Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Logiciels : Krita 4.4.1, Inkscape 1.0.2 sur Kubuntu Linux 20.04. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|True|Le saviez-vous ?
Pepper|3|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 1096 mécènes !
Pepper|5|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|6|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|7|False|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|8|False|Merci !
<hidden>|9|False|You can also translate this page if you want.
<hidden>|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
