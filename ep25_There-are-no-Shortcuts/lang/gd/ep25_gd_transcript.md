# Transcript of Pepper&Carrot Episode 25 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 25: Ruigidh each mall muileann

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sgrìobhte|1|False|Biadh CAIT

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|13|False|- Deireadh na sgeòil -
Peabar|1|True|?
Peabar|2|True|?
Peabar|3|True|?
Peabar|4|False|?
Peabar|5|True|?
Peabar|6|True|?
Peabar|7|True|?
Peabar|8|False|?
Peabar|9|True|?
Peabar|10|True|?
Peabar|11|True|?
Peabar|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|7|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
Urram|6|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 909 pàtran a thug taic dhan eapasod seo:
Urram|1|False|05/2018 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Urram|3|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Urram|4|False|Bathar-bog: Krita 4.0.0, Inkscape 0.92.3 air Kubuntu 17.10
Urram|5|False|Ceadachas: Creative Commons Attribution 4.0
Urram|2|False|Beachdan Beta: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire ’s Zveryok.
