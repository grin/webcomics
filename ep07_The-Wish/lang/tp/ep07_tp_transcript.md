# Transcript of Pepper&Carrot Episode 07 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka tu: wile

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|SOWELI KAWA O !
jan Pepa|2|False|SOWELI KAWA O !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
soweli Kawa|1|True|Ssssssuwi
soweli Kawa|2|False|Sssssuwi
soweli Kawa|5|True|Ssssssuwi
soweli Kawa|6|False|Ssssssuwi
jan Pepa|3|False|mi kama lukin e sina!
jan Pepa|4|False|o kama, o lape ala, mi o lon tomo mi!
kalama|7|False|S S S S...|nowhitespace
kalama|8|False|...SUNO !|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan sewi lili|1|True|ale pi alasa musi o... kama pona lon tomo anpa pi jan sewi lili...
jan Pepa|3|False|... wile ... ... ale mi
jan sewi lili|2|False|... mi pana sewi e ijo wan pi wile sina. wile ale sina la, mi lon e ijo ni
jan Pepa|4|False|mi... wi wile... a, mi wile e ...
soweli Kawa|5|True|mu !
soweli Kawa|6|False|Sssssuwi

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|2|True|... taso sina toki e ni ?
jan Pepa|1|True|mi sona e ni: tenpo mute la, wile pi ante ala li wile pona...
jan Pepa|3|False|"mi wile lape kin"? ken li weka ike a!
soweli Kawa|4|True|Sssssuwi
soweli Kawa|5|False|Sssssuwi
mama|8|False|tenpo April 2015 - www.peppercarrot.com - musi sitelen en toki li tan jan David Revoy - ante toki li tan jan Ret Samys
sitelen toki|6|False|lipu nanpa luka tu: wile
sitelen toki|7|False|PINI

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 273 li pana e mani la, lipu ni li lon:
mama|4|False|https://www.patreon.com/davidrevoy
mama|3|True|sina ken pana kin e pona tawa jan Pepa&soweli Kawa tawa lipu kama:
mama|7|False|ilo : lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Free/Libre li ilo Krita lon ilo Linux Mint
mama|6|False|nasin Open-source : sitelen mama suli ale li lon lipu mama, li jo e musi sitelen mute e sitelen linja.
mama|5|False|nasin lawa : Creative Commons Attribution sina ken ante, li ken pana, li ken esun, li ken pali ante...
mama|2|False|Addison Lewis ❀ A Distinguished Robot ❀ Adrian Lord ❀ Ahmad Ali ❀ Aina Reich ❀ Alandran ❀ Alan Hardman ❀ Albert Westra ❀ Alcide Alex ❀ Alexander Bülow Tomassen ❀ Alexander Sopicki ❀ Alexandra Jordan ❀ Alexey Golubev ❀ Alex Flores ❀ Alex Lusco ❀ Alex Silver Alex Vandiver ❀ Alfredo ❀ Ali Poulton (Aunty Pol) ❀ Allan Zieser ❀ Andreas Rieger ❀ Andreas Ulmer ❀ Andrej Kwadrin ❀ Andrew Andrew Godfrey ❀ Andrey Alekseenko ❀ Angela K ❀ Anna Orlova ❀ anonymous ❀ Antan Karmola ❀ Anthony Edlin ❀ Antoine Antonio Mendoza ❀ Antonio Parisi ❀ Ardash Crowfoot ❀ Arjun Chennu ❀ Arne Brix ❀ Arturo J. Pérez ❀ Aslak Kjølås-Sæverud Axel Bordelon ❀ Axel Philipsenburg ❀ barbix ❀ BataMoth ❀ Ben Evans ❀ Bernd ❀ Betsy Luntao ❀ Birger Tuer Thorvaldsen Boonsak Watanavisit ❀ Boris Fauret ❀ Boudewijn Rempt ❀ BoxBoy ❀ Brent Houghton ❀ Brett Smith ❀ Brian Behnke ❀ Bryan Butler BS ❀ Bui Dang Hai Trieu ❀ BXS ❀ carlos levischi ❀ Carola Hänggi ❀ Cedric Wohlleber ❀ Charlotte Lacombe-bar ❀ Chris Radcliff Chris Sakkas ❀ Christian Gruenwaldner ❀ Christophe Carré ❀ Christopher Bates ❀ Clara Dexter ❀ codl ❀ Colby Driedger Conway Scott Smith ❀ Coppin Olivier ❀ Cuthbert Williams ❀ Cyol ❀ Cyrille Largillier ❀ Cyril Paciullo ❀ Damien ❀ Daniel Daniel Björkman ❀ Danny Grimm ❀ David ❀ David Tang ❀ DiCola Jamn ❀ Dmitry ❀ Donald Hayward ❀ Duke ❀ Eitan Goldshtrom Enrico Billich ❀ epsilon ❀ Eric Schulz ❀ Faolan Grady ❀ Francois Schnell ❀ freecultureftw ❀ Garret Patterson ❀ Ginny Hendricks GreenAngel5 ❀ Grigory Petrov ❀ G. S. Davis ❀ Guillaume ❀ Guillaume Ballue ❀ Gustav Strömbom ❀ Happy Mimic ❀ Helmar Suschka Henning Döscher ❀ Henry Ståhle ❀ Ilyas ❀ Irina Rempt ❀ Ivan Korotkov ❀ James Frazier ❀ Jamie Sutherland ❀ Janusz ❀ Jared Tritsch JDB ❀ Jean-Baptiste Hebbrecht ❀ Jean-Gabriel LOQUET ❀ Jeffrey Schneider ❀ Jessey Wright ❀ Jim ❀ Jim Street ❀ Jiska JoÃ£o Luiz Machado Junior ❀ Joerg Raidt ❀ Joern Konopka ❀ joe rutledge ❀ John ❀ John ❀ John Urquhart Ferguson ❀ Jónatan Nilsson Jonathan Leroy ❀ Jonathan Ringstad ❀ Jon Brake ❀ Jorge Bernal ❀ Joseph Bowman ❀ Juju Mendivil ❀ Julien Duroure ❀ Justus Kat Kai-Ting (Danil) Ko ❀ Kasper Hansen ❀ Kate ❀ Kathryn Wuerstl ❀ Ken Mingyuan Xia ❀ Kingsquee ❀ Kroet ❀ Lars Ivar Igesund Levi Kornelsen ❀ Liang ❀ Liselle ❀ Lise-Lotte Pesonen ❀ Lorentz Grip ❀ Louis Yung ❀ L S ❀ Luc Stepniewski ❀ Luke Hochrein ❀ MacCoy Magnus Kronnäs ❀ Manuel ❀ Manu Järvinen ❀ Marc & Rick ❀ marcus ❀ Martin Owens ❀ Mary Brownlee ❀ Masked Admirer Mathias Stærk ❀ mefflin ross bullis-bates ❀ Michael ❀ Michael Gill ❀ Michael Pureka ❀ Michelle Pereira Garcia ❀ Mike Mosher Miroslav ❀ mjkj ❀ Nazhif ❀ Nicholas DeLateur ❀ Nicholas Terranova ❀ Nicki Aya ❀ Nicola Angel ❀ Nicolae Berbece ❀ Nicole Heersema Nielas Sinclair ❀ NinjaKnight Comics ❀ Noble Hays ❀ Noelia Calles Marcos ❀ Nora Czaykowski ❀ No Reward ❀ Nyx ❀ Olivier Amrein Olivier Brun ❀ Olivier Gavrois ❀ Omar Willey ❀ Oscar Moreno ❀ Öykü Su Gürler ❀ Ozone S. ❀ Pablo Lopez Soriano ❀ Pat David Patrick Gamblin ❀ Paul ❀ Paul ❀ Pavel Semenov ❀ Pet0r ❀ Peter ❀ Peter Moonen ❀ Petr Vlašic ❀ Philippe Jean Edward Bateman Pierre Geier ❀ Pierre Vuillemin ❀ Pranab Shenoy ❀ Pyves & Ran ❀ Raghavendra Kamath ❀ Rajul Gupta ❀ Reorx Meng ❀ Ret Samys rictic ❀ RJ van der Weide ❀ Roberto Zaghis ❀ Robin Moussu ❀ Roman Burdun ❀ Rumiko Hoshino ❀ Rustin Simons ❀ Sally Bridgewater Sami T ❀ Samuel Mitson ❀ Scott Petrovic ❀ Sean Adams ❀ Shadefalcon ❀ ShadowMist ❀ shafak ❀ Shawn Meyer ❀ Simon Forster Simon Isenberg ❀ Sonja Reimann-Klieber ❀ Sonny W. ❀ Soriac ❀ Stanislav Vodetskyi ❀ Stephanie Cheshire ❀ Stephen Bates Stephen Smoogen ❀ Steven Bennett ❀ Stuart Dickson ❀ surt ❀ Sybille Marchetto ❀ TamaskanLEM ❀ tar8156 ❀ Terry Hancock TheFaico ❀ thibhul ❀ Thomas Citharel ❀ Thomas Courbon ❀ Thomas Schwery ❀ Thornae ❀ Tim Burbank ❀ Tim J. ❀ Tomas Hajek Tom Demian ❀ Tom Savage ❀ Tracey Reuben ❀ Travis Humble ❀ tree ❀ Tyson Tan ❀ Urm ❀ Victoria ❀ Victoria White Vladislav Kurdyukov ❀ Vlad Tomash ❀ WakoTabacco ❀ Wander ❀ Westen Curry ❀ Witt N. Vest ❀ WoodChi ❀ Xavier Claude Yalyn Vinkindo ❀ Yaroslav ❀ Zeni Pong ❀ Źmicier Kušnaroŭ ❀ Глеб Бузало ❀ 獨孤欣 & 獨弧悦
