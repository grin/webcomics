# Transcript of Pepper&Carrot Episode 13 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka tu wan: tenpo musi pi lape kama

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|4|False|ma sina ni li pona a!
jan Pepa|5|False|kama mi li pona tan sina, jan Kowijana o!
jan Pepa|3|False|... nanpa wan !
jan Pepa|1|True|tenpo ...
jan Pepa|2|True|... pi pali ala ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kowijana|1|True|pona !
jan Kowijana|2|False|mi wile e ni: sina pilin ike ala tan jan pali mi
jan Pepa|11|False|a, ale li pona! ona li pona tawa mi. pilin la, tomo sina li sama tomo mi.
jan Sisimi|13|False|... li jo e wan mute, li pilin pona lon tenpo sama!
jan Pepa|14|False|ni li lon!
jan Sisimi|12|True|insa pi tomo sina li pona lukin mute tawa mi...
sewi ike|9|False|UuuuUuu ! ! !|nowhitespace
sewi ike|10|False|UuUuu ! ! !|nowhitespace
sewi ike|8|False|UuuuUuu ! ! !|nowhitespace
kalama|7|False|Uuuuuuuutala !|nowhitespace
kalama|6|False|Kiwen!|nowhitespace
kalama|5|False|Pakala!|nowhitespace
kalama|3|False|Sssselii ! ! !|nowhitespace
kalama|4|False|Ssuno ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kowijana|2|True|... tenpo mute la, tomo mi li pilin pi ...
jan Sisimi|5|True|wile mi la, tomo mi li sama suli pona pi tomo sina,
kalama|7|False|Wwaalo ! ! !|nowhitespace
kalama|4|False|Noka !|nowhitespace
jan Sisimi|8|False|... "jan-sona-pi-kulupu-Aa-li-nasin-ni-ala" .
jan Pepa|9|True|a a a !...
jan Pepa|10|True|nasin li pali pilin...
jan Pepa|11|False|jan sona mi li toki sama ni!
jan Sisimi|12|False|a ?
jan Kowijana|1|True|toki sina pi pana pilin li pona ...
jan Kowijana|3|False|... "suli ala" tawa mi ?...
jan Sisimi|6|False|taso ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kowijana|2|False|jan o, lukin mute. mi ken kama tawa pini!
jan Pepa & jan Sisimi|1|False|aa-a a a a!
jan Pepa|3|True|a, mi lukin ... !
jan Pepa|5|False|... kin la, kama ni li lon tenpo kama: ona li...
kalama|6|False|WWWwaaawwaaa ! ! !|nowhitespace
kalama|9|False|Ssssss|nowhitespace
jan Sisimi|8|False|?!!
jan Kowijana|7|False|JAN PEPA O! !!|nowhitespace
jan Pepa|4|True|sina o toki e ni: alasa musi ni li pali lili taso...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|6|False|P A K A LA ! !|nowhitespace
jan Sisimi|8|False|IKE A!
jan Kowijana|9|False|ni li sin nanpa seme?
jan Pepa|11|False|o anpa e ona ! o pini e ni!
jan Kowijana|1|False|...ike a! tenpo li pini a! ona... ona li...
jan Sisimi|2|False|ala aa aa !!!|nowhitespace
sewi ike|3|False|AAAA A A A A A A !!!|nowhitespace
jan Sisimi|4|False|aaa ! ! !|nowhitespace
jan Kowijana|5|False|SINA KAMA PAKALA TAN NI ! ...
kalama|7|False|PAKALA ! !|nowhitespace
kalama|13|False|N O KA ! !|nowhitespace
kalama|12|False|N OKA ! !|nowhitespace
jan Pepa|10|True|SOWELI KAWA O!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|10|False|tenpo 11/2015 - musi sitelen & toki li tan jan David Revoy - ante toki li tan jan Ret Samys
kalama|1|False|anpa|nowhitespace
jan Pepa|4|True|mi sona e ni: sina lukin awen e mi...
jan Pepa|3|True|a, suwi mi o, pilin ike ala!
jan Pepa|5|False|... taso mi awen, ni li musi taso!
soweli Kawa|8|False|Mmmu
sitelen|6|False|Citadels & Phoenixes
sitelen|2|False|jan lawa lili Kowijana
sitelen toki|9|False|- PINI -
sitelen|7|False|Citadels & Phoenixes

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 602 li pana e mani la, lipu ni li lon:
mama|2|True|sina ken pana kin e pona tawa lipu kama pi jan Pepa&soweli Kawa:
mama|3|False|https://www.patreon.com/davidrevoy
mama|4|False|nasin lawa : Creative Commons Attribution 4.0 mama pali li lon lipu www.peppercarrot.com ilo : sitelen pi lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Libre li ilo Krita 2.9.9, li ilo Inkscape 0.91 lon Linux Mint 17
