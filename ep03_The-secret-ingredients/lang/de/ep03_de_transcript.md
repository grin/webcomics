# Transcript of Pepper&Carrot Episode 03 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 3: Die geheimen Zutaten

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|Komona City; Markttag
Pepper|2|False|Guten Morgen, Sir, ich hätte gerne acht Stück von den Kürbissternen
Verkäufer|3|False|Bitteschön, das macht 60Ko*
Pepper|5|False|...auweia
Fußnote|4|False|* Ko = die Währungseinheit von Komona

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Ähm, tut mir leid, ich kann doch nur vier nehmen...
Verkäufer|2|False|Grrr...
Saffron|3|False|Ich grüße Sie, Sir. Bitte stellen Sie mir je zwei Dutzend von allem zusammen. Premium-Qualität, wie üblich.
Verkäufer|4|False|Immer eine Freude, Sie zu bedienen, Miss Saffron
Saffron|5|False|Schau an, da ist ja Pepper
Saffron|6|False|Oh, lass mich raten - das Geschäft auf dem Lande läuft prima?
Pepper|7|False|...
Saffron|8|False|Ich vermute, du bereitest die Zutaten für den morgigen Zaubertrank-Wettbewerb vor?
Pepper|9|True|... ein Zaubertrank-Wettbewerb ?
Pepper|10|False|... morgen ?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|So ein Glück - ich habe noch einen ganzen Tag Zeit zur Vorbereitung!
Pepper|5|False|Auf geht's - das gewinne ich!
Schrift|1|False|Komona Zaubertrank-Wettbewerb
Schrift|2|False|50 000Ko Hauptpreis FÜR DEN BESTEN TRANK
Schrift|3|False|Am Azartag, 3 Pinkmond Großer Platz von Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Oh ... ! Ich hab's ...
Pepper|3|False|... das ist genau das, was ich brauche!
Pepper|4|True|Carrot !
Pepper|5|False|Wach auf, wir werden die Zutaten gemeinsam beschaffen.
Pepper|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Zuerst brauche ich einige Nebelperlen aus diesen schwarzen Wolken...
Pepper|2|False|...und ein paar rote Beeren aus dem ver-wunschenen Urwald

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...dazu Phönix-Eierschalen aus dem Tal der Vulkane...
Pepper|2|False|...und zuletzt noch einen Tropfen Milch von einer jungen Drachenkuh

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Das war's, Carrot - ich denke, ich habe alles, was ich brauche
Pepper|2|True|Das sieht...
Pepper|3|False|...perfekt aus
Pepper|4|True|Mmm ...
Pepper|5|True|Bester ... Kaffee aller Zeiten !
Pepper|6|False|Das ist alles, was ich brauche, um die ganze Nacht zu arbeiten und den besten Zaubertrank für den morgigen Wettbewerb zu brauen
Erzähler|7|False|Fortsetzung folgt…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams, Shadefalcon,
Impressum|2|False|Diese Episode würde nicht existieren ohne die Unterstützung meiner 93 Förderer
Impressum|1|False|Dieser Webcomic ist komplett frei und Open Source ( Creative Commons Namensnennung 3.0 Unported, hochauflösende Quelldateien zum Download verfügbar )
Impressum|3|False|https://www.patreon.com/davidrevoy
Impressum|6|False|Besonderen Dank an : Amireeti, David Tschumperlé (G'MIC) und das gesamte Krita Team ! Deutsche Übersetzung : Helmar Suschka / Alexandra Jordan
Impressum|7|False|Diese Episode wurde zu 100% mit freien Open Source Tools erstellt Krita und G'MIC auf Xubuntu (GNU/Linux)
Pepper|5|False|Vielen Dank !
