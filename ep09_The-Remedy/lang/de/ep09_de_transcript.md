# Transcript of Pepper&Carrot Episode 09 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 9: Das Heilmittel

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|1|False|Der Tag nach der Feier*...
Pepper|3|True|Hmm? Die Zeichen der Berge, des Waldes und der Wolken?
Pepper|4|False|Aber wieso?
Pepper|5|False|Oh nein!
Fußnote|2|False|* siehe Episode 8: Peppers Geburtstagsfeier

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Ich hatte total vergessen, mein „Sicherheitssystem“ abzuschalten
Pepper|5|False|Es hat wohl ein wenig geregnet, glaube ich...
Pepper|4|True|Mal abgesehen davon, wie war deine Nacht?
Pepper|6|True|...tut mir echt leid, aber ist ja nichts passiert, oder?
Pepper|7|False|Ich habe einen Tee vorbereitet, um uns nach diesem Missgeschick aufzuheitern!
Pepper|1|True|'Tschuldigung
Pepper|3|True|...aber ich bin so froh, dass ihr alle gekommen seid!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Dumme Pepper! Jetzt ist es zu spät und sie hassen mich alle...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Yuzu|10|True|Wuu
Yuzu|11|False|Uhf
Carrot|1|False|Mie-Au-Au
Trüffel|15|True|Mieoh
Trüffel|16|False|Mieoh
Mango|13|True|Gluck
Mango|14|False|Gluck
Saffron|18|False|Ins Labor! Das kann tödlich sein!
Saffron|17|True|Schnell!
Yuzu|2|True|Wuuf
Yuzu|3|True|UUf
Carrot|12|False|Mieouou
Trüffel|5|True|MieOh
Mango|7|True|Gluck
Mango|8|True|Gluck
Trüffel|6|False|MieOh !
Mango|9|False|Gluck !
Yuzu|4|False|Wuuf !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|False|...trotzdem schade, dass wir die Nebenwirkungen des Tranks nicht vermeiden konnten...
Shichimi|6|False|Keine Sorge, der Fell-und Federausfall ist nur vorübergehend, das wächst schnell nach
Erzähler|7|False|- ENDE -
Impressum|8|False|Juli 2015 - Grafik & Handlung: David Revoy - Übersetzung: Helmar Suschka & Alexandra Jordan
Saffron & Pepper|1|True|Spitze!
Shichimi & Coriander|3|True|Yippee!
Saffron & Pepper|2|False|Wir sind die Besten!
Shichimi & Coriander|4|False|Wir haben sie geheilt!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 406 Förderer:
Impressum|7|False|https://www.patreon.com/davidrevoy
Impressum|6|True|Du kannst die nächste Episode von Pepper&Carrot hier unterstützen:
Impressum|8|False|Lizenz : Creative Commons Namensnennung 4.0 Quelldaten : verfügbar auf www.peppercarrot.com Software : Diese Episode wurde zu 100% mit freier Software erstellt Krita 2.9.6, Inkscape 0.91 auf Linux Mint 17
Impressum|4|False|Глеб Бузало ★ 无名 ★ 獨孤欣 & 獨弧悦 ★ Adam Mathena ★ Addison Lewis A Distinguished Robot ★ Aina Reich ★ Alan Hardman ★ Albert Westra ★ Alex ★ AlexanderKennedy Alexander Lust ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex V ★ Alfredo Alien Green ★ Allan Zieser ★ Alok Baikadi ★ Amic ★ Andreas Rieger ★ Andreas Ulmer ★ Andrej Kwadrin Andrew Godfrey ★ Andrey Alekseenko ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Ardash Crowfoot Arjun Chennu ★ Arnulf ★ Arturo J. Pérez ★ Austin Knowles ★ Axel Bordelon ★ Bastian Hougaard ★ Ben Evans blacksheep33512 ★ Boonsak Watanavisit ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Bryan Butler ★ BS Bui Dang Hai Trieu ★ carlos levischi ★ carolin der keks ★ Cedric Wohlleber ★ Chance Millar ★ Charles★ Chaz Straney Chris ★ Chris Sakkas ★ Christian Howe ★ Christophe Carré ★ Christopher Bates ★ Christopher Rodriguez Christopher Vollick ★ Colby Driedger ★ Damien ★ Daniel ★ Daniel Lynn ★ Danijel ★ David Brennan David Kerdudo ★ David Tang ★ DecMoon ★ Derek Zan ★ Dio Fantasma ★ Dmitry ★ Doug Moen ★ douze12 ★ Drew Fisher Durand D’souza ★ Elijah Brown ★ Elisha Condon ★-epsilon-★ Eric Schulz ★ Faolan Grady ★ francisco dario aviltis Francois Schnell ★ Francou ★ Garret Patterson ★ Gary Thomas ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov Grzegorz Wozniak ★ G. S. Davis ★ Guillaume ★ Gustav Strömbom ★ happy2ice ★ Happy Mimic ★ Helmar Suschka Henning Döscher ★ Henry Ståhle ★ HobbyThor ★ Ilyas ★ Irina Rempt ★ Jacob ★ James Frazier ★ Jamie Sutherland ★ Janusz Jason ★ Jeffrey Schneider ★ Jessey Wright ★ Jessica Gadling ★ Joao Luiz ★ John ★ John Gholson ★ John Urquhart Ferguson Jonas Peter ★ Jonathan Leroy ★ Jonathan Walsh ★ Justus Kat ★ Kailyce ★ Kai-Ting (Danil) Ko ★ Kari Lehto ★ Kathryn Wuerstl kazakirinyancat ★ Ken Mingyuan Xia ★ Kevin Estalella ★ Kevin Trévien ★ Kingsquee ★ Kurain ★ La Plume ★ Lenod ★ Liang Lise-Lotte Pesonen ★ Lloyd Ash Pyne ★ Lorentz Grip ★ Lorenzo Leonini ★ Magnus Kronnäs ★ Marc et Rick ★ Marco ★ marcus Matt Lichtenwalner ★ Michael F. Schönitzer ★ Michael Gill ★ Michael Polushkin ★ Mike Mosher ★ Mohamed El Banna ★ Nabispace Nazhif ★ Nicholas DeLateur ★ Nicola Angel ★ Oleg Schelykalnov ★ Olga Bikmullina ★ Olivier Amrein ★ Olivier Gavrois ★ Omar Willey Oscar Moreno ★ Pato Acevedo ★ Patrick Dezothez ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Peter Moonen ★ Petr Vlašic Pierre Vuillemin ★ Pranab Shenoy ★ Pummie ★ Raghavendra Kamath ★ Rajul Gupta ★ Ramel Hill ★ Ray Brown ★ Rebecca Morris ResidentEvilArtist ★ Reuben Tracey ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Ryan ★ Sally Bridgewater Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Scott Smiesko ★ ShadowMist ★ shafak ★ Shawn Meyer ★ Soriac ★ Stanislav ★ Stephan Theelke Stephen Bates ★ Steven Bennett ★ Stuart Dickson ★ surt ★ Takao Yamada ★ TamaskanLEM ★ tar8156 ★ TheFaico ★ thibhul ★ Thomas Schwery T.H. Porter ★ Tim Burbank ★ Tim J. ★ Tom Savage ★ Travis Humble ★ Tristy ★ Tyson Tan ★ Venus ★ Vera Vukovic ★ Victoria ★ Victoria White WakoTabacco ★ Wei-Ching Shyu ★ Westen Curry ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Yasmin ★ Zeni Pong
Impressum|5|False|Adrian Lord ★ Ahmad Ali ★ al ★ Alandran ★ Alcide ★ Alexander Bülow Tomassen ★ Alexander Kashev ★ Alex Bradaric ★ Alex Cochrane ★ Alexey Golubev Ali Poulton (Aunty Pol) ★ Amy ★ Andrew ★ Andy Gelme ★ Angelica Beltran ★ anonymous ★ Antoine ★ Antonio Mendoza ★ Antonio Parisi ★ Axel Philipsenburg barbix ★ BataMoth ★ Bela Bargel ★ Bernd ★ Bernhard Saumweber ★ Betsy Luntao ★ Birger Tuer Thorvaldsen ★ blueswag ★ Boris Fauret ★ Brett Bryan Rosander ★ BXS ★ Chris Kastorff ★ Chris Radcliff ★ Christian Gruenwaldner ★ Clara Dexter ★ codl ★ Comics by Shoonyah Studio ★ Conway Scott Smith Coppin Olivier ★ Craig Bogun ★ Crystal Bollinger ★ Cuthbert Williams ★ Cyol ★ Cyrille Largillier ★ Cyril Paciullo ★ Daniel Björkman ★ Dan Norder Dan Stolyarov ★ David ★ Davi Na ★ Dawn Blair ★ Deanna ★ Denis Bolkovskis ★ Dezponia Veil ★ DiCola Jamn ★ Dmitriy Yakimov ★ Donald Hayward Douglas Oliveira Pessoa ★ Duke ★ Eitan Goldshtrom ★ Emery Schulz ★ Enrico Billich ★ Erik Moeller ★ Esteban Manchado Velázquez ★ Fen Yun Fat Fernando Nunes ★ ida nilsen ★ Igor ★ Ivan Korotkov ★ Jamie Hunter ★ Jason Baldus ★ Jazyl Homavazir ★ JDB ★ Jean-Baptiste Hebbrecht Jean-Gabriel LOQUET ★ Jhonny Rosa ★ Jim ★ Jim Street ★ Joerg Raidt ★ Joern Konopka ★ joe rutledge ★ John ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman Josh ★ Josh Cavalier ★ Juju Mendivil ★ Julian Dauner ★ Julia Velkova ★ Kate ★ Kroet ★ Lars Ivar Igesund ★ Liselle ★ Liska Myers ★ Louis Yung Luc Stepniewski ★ Luke Hochrein ★ Mahwiii ★ Mancy S ★ Manu Järvinen ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Megan Sandiford ★ Michael Michael Pureka ★ Michelle Pereira Garcia ★ Miroslav ★ mjkj ★ Moonsia ★ Moritz Fuchs ★ Muriah Summer ★ Mylène Cassen ★ Nicholas Terranova Nicole Heersema ★ Nielas Sinclair ★ Nikita Stafeev ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ No Reward ★ Nyx ★ Öykü Su Gürler Ozone S. ★ Pat David ★ Patrick Kennedy ★ Paul ★ Pet0r ★ Peter ★ Peter ★ Pierre Geier ★ Pyves & Ran ★ Ray Cruz ★ Raymond Fullon ★ Ray Powell Rebecca ★ Rebekah Hopper ★ Rei ★ Reorx Meng ★ Ret Samys ★ rictic ★ Robin Moussu ★ Sean Adams ★ Sebastien ★ Sevag Bakalian ★ Shadefalcon Simon Isenberg ★ Simon Moffitt ★ Siora ★ Sonja Reimann-Klieber ★ Sonny W. ★ Stanislav German-Evtushenko ★ Stephen Smoogen ★ Surabhi Gaur Taedirk ★ Tas Kartas ★ Terry Hancock (Personal) ★ Thomas Citharel ★ Thomas Werner ★ Thor Galle ★ Thornae ★ Timothy Boersma ★ Tomas Hajek Tomáš Slapnička ★ Tom Dickson ★ tree ★ uglyheroes ★ Umbra Draconis ★ Vitaly Tokarenko ★ Vladislav Kurdyukov ★ Wander ★ Wilhelmine Faust William Crumpler ★ Źmicier Kušnaroŭ ★ zubr kabbi
Impressum|3|False|Arne Brix ★ Boudewijn Rempt ★ Brent Houghton ★ Casey Tatum Davis Aites ★ Ejner Fergo ★ Enrique Lopez ★ Francois Didier freecultureftw ★ Jonathan Ringstad ★ Julio Avila ★ Levi Kornelsen Matthew Reynolds ★ Mefflin Ross Bullis-bates ★ Michael Oliveira Nguyen Minh Trung ★ Nicki Aya ★ NinjaKnight Comics ★ Olivier Brun Praveen Bhamidipati ★ Ricardo Muggli ★ RJ van der Weide Roman Burdun ★ Urm
Impressum|2|False|Jónatan Nilsson Alex Kotenko ★ Philippe Jean Edward Bateman
