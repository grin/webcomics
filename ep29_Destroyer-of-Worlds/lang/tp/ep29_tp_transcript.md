# Transcript of Pepper&Carrot Episode 29 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute luka tu tu: pakala pi ma ale

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sewi ike|2|True|pini pina la!
sewi ike|3|False|pakala pi poki mute li lon!
sewi ike|4|False|ken suli la, ni li tan nasin tenpo suli pi tawa mun!
sewi ike|5|True|o suli! o suli, pakala lili o!
sewi ike|6|False|o weka e len tan ma sin ni. mi wile ANPA e ona. mi wile LAWA e ona.!
sewi ike|7|True|o lukin…
sewi ike|8|False|ni li musi a.
sewi ike|9|False|tenpo suno ni li wile kama tenpo suno mi…
sitelen toki|1|False|tenpo sama la, poki ante la…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sewi ike|1|False|poki ni la, soweli li ken sona!
sewi ike|2|False|o tawa, pakal lili o!
sewi ike|3|False|o suli! aaa a a a a a a a a!
jan Pepa|4|True|sewi a!
jan Pepa|5|False|ni li tenpo musi suli a, jan pona mi o!
jan Kowijana|6|False|jan Pepa o, jan Sapon en mi li sona ala e nasin pi wawa nasa Pakalaa sina.
jan Sapon|7|False|sona ni li len tawa mi. sina ken ala ken toki e ona?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|a a a, ni li wile kepeken toki mute.
jan Pepa|2|True|a, nasin la, jan o sona e lawa pi nasin pi ken ale...
jan Pepa|3|False|…tan ijo suli, tan ijo lili.
jan Kowijana|4|False|a, ni li sona lon tenpo seme?
jan Pepa|7|False|o awen. mi pana e ijo pona tawa ni.
jan Pepa|8|False|o awen, mi kama kepeken sona Pakalaa…
jan Pepa|9|False|mi lukin!
kalama|5|True|LUKA
kalama|6|False|LUKA
jan Pepa|10|True|o lukin e palisa lili pi uta jan. ona li lon ma lon insa kiwen tu.
jan Pepa|11|False|mi weka e ona tan ma la, jan li pakala ala e noka ona tan ni.
jan Pepa|12|True|ante lili ni li lon nasin suli pi ken ale la, kama suli li ken..
jan Pepa|13|False|ni li nasin Pakalaa!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Sisimi|1|False|jaki!
jan Kowijana|2|False|jaki aa! ijo ni li tan uta jan!
jan Sapon|3|False|a a a! ni li suli sama tenpo ale, jan Pepa o!
jan Kowijana|4|False|jan Pepa o, pona li tan... "sona" pana sina.
jan Kowijana|5|True|tenpo ni la mi ale o lape, anu seme?
jan Kowijana|6|False|sama la, jan wan o telo e luka ona.
jan Pepa|7|False|jan o! o awen!
kalama|8|False|Weka!
kalama|9|True|Luka!
kalama|10|False|Luka!
kalama|11|False|Utala!|nowhitespace
soweli Kawa|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|False|Tawwwa!
kalama|2|False|Utala!
kalama|3|False|Insa!
kalama|4|False|Pakala!|nowhitespace
kalama|5|False|Sssseli…|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|We have a patch for the artwork in case your text is too short or is not easily split in two words. Ask David or Midgard for more information. Look at the Korean version for an example.
<hidden>|0|False|NOTE FOR TRANSLATORS
sewi ike|7|False|O UTALA!!!
sewi ike|6|True|aaa a a a a a AA! pini pona a!
sewi ike|9|False|!?
sitelen|1|True|AWEN PI
sitelen|2|False|MUSI SELI
kalama|3|False|Sss elli ! !|nowhitespace
kalama|4|False|Seli i ! !|nowhitespace
kalama|5|False|KALAMA!
kalama|8|False|Tawwwa! !|nowhitespace

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|3|False|Suno!|nowhitespace
kalama|2|False|K ALAM A!|nowhitespace
kalama|1|False|P AKAL A !|nowhitespace
kalama|4|False|Walo! !|nowhitespace
kalama|5|False|WaW A !|nowhitespace
jan Pepa|6|True|o pilin ike ala, suwi Kawa o.
jan Pepa|7|False|kute la, jan li awen e tenpo musi.
jan Pepa|8|False|o lape pona suwi a!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sewi ike|5|False|...
jan Kumin|1|False|seme a? ona li kama e mute suli ni, taso ona li sona ala e ni anu seme?
jan Kajen|2|False|kin a. ni li sona.
jan Tume|3|False|jan o, pilin mi la, jan Pepa mi li kama pini pona!
kalama|6|False|Pini!
kalama|4|False|Konn nn ! !|nowhitespace
sitelen toki|7|False|- TOKI TU WAN PI KAMA LAWA KOWIJANA LI PINI -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|5|True|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa. nimi sina li ken lon ni!
jan Pepa|3|True|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute.
jan Pepa|4|False|jan 960 li pana e mani la, lipu ni li lon!
jan Pepa|7|True|sina wile sona la, o lukin e lipu www.peppercarrot.com !
jan Pepa|6|True|ilo Patreon en ilo Tipeee en ilo PayPal en ilo Liberapay en ilo ante la, mi lon!
jan Pepa|8|False|sina pona!
jan Pepa|2|True|sina sona ala sona?
mama|1|False|tenpo April 25, 2019 musi sitelen & toki: jan David Revoy. jan pi lukin pona: jan CalimeroTeknik en jan Craig Maloney en jan Martin Disch en jan Midgard en jan Nicolas Artance en jan Valvin. toki pona ante toki: jan Ret Samys . musi ni li tan pali pi ma Elewa mama: jan David Revoy. jan awen nanpa wan: jan Craig Maloney. jan pi toki sitelen: jan Craig Maloney en jan Nartance en jan Scribblemaniac en jan Valvin. jan pi pona pali: Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson . ilo: ilo Krita 4.1.5~appimage en ilo Inkscape 0.92.3 li kepeken ilo Kubuntu 18.04.1. nasin pi lawa jo: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
