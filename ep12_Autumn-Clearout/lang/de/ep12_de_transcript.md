# Transcript of Pepper&Carrot Episode 12 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 12: Herbstputz

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|9|True|Kling
Geräusch|10|True|Klang
Geräusch|11|False|Klong
Cayenne|1|False|Lachtränke
Cayenne|2|False|Mega Haarwachstränke
Cayenne|3|False|Stinkblasentränke
Cayenne|4|False|"Klarsicht" Tränke
Cayenne|5|False|Rauchtränke ...
Cayenne|6|False|... nur um ein paar zu nennen!
Pepper|7|True|Ja, Ich weiß:
Pepper|8|False|"Eine-echte-Hexe-von-Chaosāh-würde-solche-Tränke-nicht-brauen".
Cayenne|13|True|Ich muss auf den Markt von Komona.
Cayenne|14|False|Lass das alles hier verschwinden, während ich weg bin. Es wäre nicht gut, wenn kleine Scherzbolde sie finden und damit rumspielen.
Cayenne|12|False|Genau. So sollte es sein.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|2|False|Wuuusch !|nowhitespace
Pepper|3|False|Mist!
Pepper|4|True|Alles vergraben ?!
Pepper|5|False|Aber das dauert Stunden, und erst die Blasen, die wir danach haben werden!
Geräusch|6|False|Buum
Pepper|1|False|Lass es verschwinden? Einfach! Jippi!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|"Unter keinen Umständen mit Magie. Eine echte Hexe von Chaosāh benutzt ihre Magie nicht für die täglichen Aufgaben."
Pepper|2|False|Grrr !
Pepper|3|True|CARROT!
Pepper|4|False|Das siebte Buch von Chaosāh: "Gravitationsfelder"... ...sofort!
Pepper|5|False|Ich zeig dieser alten Wichtigtuerin, was eine echte Hexe von Chaosāh ist!
Geräusch|6|True|Tam
Geräusch|7|False|Tam

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|2|False|VRO O oo oo oo oo oo|nowhitespace
Cayenne|3|False|... Ein Schwarzes Chaosāh-Loch?!...
Pepper|4|False|?!
Cayenne|5|False|... Das verstehst Du also, wenn ich sage: "keine Magie"?
Pepper|6|False|... Hey... Solltest du nicht auf dem Markt sein?
Cayenne|7|True|Natürlich nicht!
Cayenne|8|False|Und ich hatte recht. Man kann dich nicht einen Moment aus den Augen lassen!
Pepper|1|False|GURGES ATER!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|wo oo wo owo owoo|nowhitespace
Geräusch|7|False|wo oo wo owo owoo|nowhitespace
Geräusch|6|False|Bamm
Cayenne|8|True|Und hier hast du's: Fast alles wird von stabilen Umlaufbahnen oder Lagrange-Punkten angezogen und schwebt konstant umher.
Carrot|10|False|?!
Cayenne|2|True|... Schau gut zu.
Cayenne|3|True|Unzureichende Masse!
Cayenne|4|True|Zu wenig Unterschied im Gravitationsfeld!
Cayenne|5|False|Selbst die zentralste stabile Umlaufbahn ist zu klein!
Cayenne|9|False|Selbst ein etwas größerer Ereignishorizont hätte das Kunststück vollbracht!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|! ! !|nowhitespace
Cayenne|2|False|! ! !|nowhitespace
Cayenne|3|False|FLUCHUS ANNULLARE MAXIMUS!
Geräusch|4|False|Sch Klak!|nowhitespace
Erzähler|5|False|- ENDE -
Impressum|6|False|10/2015 - Grafik & Handlung: David Revoy - Übersetzung: Philipp Hemmer

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|https://www.patreon.com/davidrevoy
Impressum|3|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 575 Förderer:
Impressum|2|True|Lizenz : Creative Commons Namensnennung 4.0 Quelldaten : verfügbar auf www.peppercarrot.com Software : Diese Episode wurde zu 100% mit freier Software erstellt Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 auf Linux Mint 17
Impressum|4|False|Du kannst die nächste Episode von Pepper&Carrot hier unterstützen:
