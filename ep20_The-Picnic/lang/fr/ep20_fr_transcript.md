# Transcript of Pepper&Carrot Episode 20 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 20 : Le Pique-nique

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|False|- FIN -
Crédits|2|False|12/2016 - www.peppercarrot.com - Dessin & Scénario : David Revoy
Crédits|3|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Crédits|4|False|Licence : Creative Commons Attribution 4.0, Logiciels: Krita 3.1, Inkscape 0.91 sur Manjaro XFCE

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 825 Mécènes :
Crédits|2|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
