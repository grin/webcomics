# Transcript of Pepper&Carrot Episode 20 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa mute: moku lon ma

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|1|False|- PINI -
mama|2|False|tenpo 12/2016 - www.peppercarrot.com - musi sitelen & toki li tan jan David Revoy - ante toki pi toki pona li tan jan Ret Samys
mama|3|False|musi ni li tan pali pi ma Elewa. pali ni li tan jan David Revoy. jan Craig Maloney li pana e pona tawa ona. jan Willem Sonke en jan Moini en jan Hali en jan CGand en jan Alex Gryson li pona e musi ni.
mama|4|False|nasin lawa : Creative Commons Attribution 4.0, ilo: ilo Krita 3.1 en ilo Inkscape 0.91 li kepeken ilo Manjaro XFCE

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li kepeken nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 825 li pana e mani la, lipu ni li lon:
mama|2|False|sina kin li ken pana e pona tawa jan Pepa&soweli Kawa lon www.patreon.com/davidrevoy
